﻿#include <QCoreApplication>

#include <QDebug>

#ifdef Q_OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif


class Sample
{
public:

	//デストラクタ
	~Sample(){
		qDebug() << u8"デストラクタが呼ばれました";
		delete m_pArrayValue;
	}

	//デフォルトコンストラクタ
	Sample():m_pArrayValue(new int[3]){
		qDebug() << u8"デフォルトコンストラクタが呼ばれました";
		qDebug() << "address of this m_pArrayValue =" << &this->m_pArrayValue;
	}

	//初期値指定コンストラクタ
	Sample(int value):m_pArrayValue(new int[3])
	{
		for(size_t i = 0; i < 3; ++i)
			m_pArrayValue[i] = value;
		qDebug() << u8"初期値指定コンストラクタが呼ばれました";
		qDebug() << "address of this m_pArrayValue =" << &this->m_pArrayValue;
	}

//コピーコンストラクタをちゃんと定義しないと死にます。
//理由は、コンパイラが生成するコピーコンストラクタやコピー代入演算子は
//ポインタメンバに対しては、アドレス値をコピーするのみで指しているオブジェクトはコピーしてくれない
//このため、デストラクタで片方がオブジェクトを消してしまうと、もう片方がすでに消えてしまったオブジェクトの
//アドレスを保持していて、これに対してdeleteが発生してしまうため。
#if 1
	//コピーコンストラクタ
	Sample(const Sample& rhs):m_pArrayValue(new int[3])
	{
		this->m_strObjectName = rhs.m_strObjectName;

		for(size_t i = 0; i < 3; ++i)
			this->m_pArrayValue[i] = rhs.m_pArrayValue[i];
		qDebug() << u8"コピーコンストラクタが呼ばれました";
		qDebug() << "address of this m_pArrayValue =" << &this->m_pArrayValue;
		qDebug() << "address of rhs m_pArrayValue =" << &rhs.m_pArrayValue;
	}

	//コピー代入演算子 あれ？これって自己代入に対して安全？　いまのところうまくいっているようだ。
	Sample& operator = (const Sample& rhs)
	{
		this->m_strObjectName = rhs.m_strObjectName;

		for(size_t i = 0; i < 3; ++i)
			this->m_pArrayValue[i] = rhs.m_pArrayValue[i];
		qDebug() << u8"コピー代入演算子が呼ばれました";
		qDebug() << "address of this m_pArrayValue =" << &this->m_pArrayValue;
		qDebug() << "address of rhs m_pArrayValue =" << &rhs.m_pArrayValue;

		return *this;
	}
#endif

//private:	サンプルなので隠ぺいをはずす
	int* m_pArrayValue;
	QString m_strObjectName;
};

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	//デフォルトコンストラクタ
	qDebug() << u8"s1生成(初期値指定なし)";
	Sample s1;
	s1.m_strObjectName = "s1";

	qDebug() << u8"s1の配列に値を設定";
	s1.m_pArrayValue[0] = 2;
	s1.m_pArrayValue[1] = 4;
	s1.m_pArrayValue[2] = 6;

	//初期値を与えるコンストラクタ
	qDebug() << u8"s2生成(初期値5指定)";
	Sample s2(5);
	s2.m_strObjectName = "s2";

	//s1からコピーコンストラクタを呼ぶ
	qDebug() << u8"s3生成(=s1指定)";
	Sample s3 = s1;
	qDebug() << "s3.ObjectName :" << s3.m_strObjectName;
	s3.m_strObjectName = "s3";
	qDebug() << "s3.ObjectName(revise) :" << s3.m_strObjectName;

	//s2からコピーコントラクタを呼ぶ
	qDebug() << u8"s4生成(s2からコピーコンストラクト)";
	Sample s4 = s2;
	qDebug() << "s4.ObjectName :" << s4.m_strObjectName;
	s4.m_strObjectName = "s4";
	qDebug() << "s4.ObjectName(revise) :" << s4.m_strObjectName;

	qDebug() << u8"s2の配列に値をサンプルなので再設定";
	s2.m_pArrayValue[0] = 3;
	s2.m_pArrayValue[1] = 6;
	s2.m_pArrayValue[2] = 9;

	//結果表示
	qDebug() << "index\ts1\ts2\ts3\ts4";
	for(size_t i = 0; i < 3; ++i)
	{
		qDebug() << i << "\t" << s1.m_pArrayValue[i] << "\t" << s2.m_pArrayValue[i] << "\t"
					 << s3.m_pArrayValue[i] << "\t" << s4.m_pArrayValue[i];
	}

	//s1にコピー代入演算子でs2を代入する
	qDebug() << u8"s1 = s2 (代入)";
	s1 = s2;
	qDebug() << "s1.ObjectName :" << s1.m_strObjectName;
	s1.m_strObjectName = "s1";
	qDebug() << "s1.ObjectName(revise) :" << s1.m_strObjectName;


	//結果表示
	qDebug() << "index\ts1\ts2\ts3\ts4";
	for(size_t i = 0; i < 3; ++i)
	{
		qDebug() << i << "\t" << s1.m_pArrayValue[i] << "\t" << s2.m_pArrayValue[i] << "\t"
					 << s3.m_pArrayValue[i] << "\t" << s4.m_pArrayValue[i];
	}

	//自己代入
	 qDebug() << u8"s1 = s1 (自己代入)";
	 //結果表示
	 qDebug() << "index\ts1\ts2\ts3\ts4";
	 for(size_t i = 0; i < 3; ++i)
	 {
		 qDebug() << i << "\t" << s1.m_pArrayValue[i] << "\t" << s2.m_pArrayValue[i] << "\t"
					  << s3.m_pArrayValue[i] << "\t" << s4.m_pArrayValue[i];
	 }

	return 0;//a.exec();
}
