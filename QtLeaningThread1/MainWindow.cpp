#include "MainWindow.h"
#include "ui_MainWindow.h"
//#include "ProcessFunctions.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_action_Quit_triggered()
{
	emit sigQuit();
}

void MainWindow::on_btnStart_clicked()
{
	//int val = sampleProcess1();
	//qDebug() << "result =" << val;
	emit sigStart();
}
