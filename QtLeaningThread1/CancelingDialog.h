#ifndef CANCELINGDIALOG_H
#define CANCELINGDIALOG_H

#include <QDialog>

namespace Ui {
class CancelingDialog;
}

class CancelingDialog : public QDialog
{
	Q_OBJECT

public:
	explicit CancelingDialog(QWidget *parent = 0);
	~CancelingDialog();

private:
	Ui::CancelingDialog *ui;
};

#endif // CANCELINGDIALOG_H
