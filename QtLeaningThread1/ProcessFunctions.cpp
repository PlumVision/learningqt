#include "ProcessFunctions.h"
#include <QDebug>

int sampleProcess1()
{
	int numElement = 1280 * 1024;
	int* sampleArray = new int[numElement];
	memset(sampleArray, 0, sizeof(int)*(numElement));

	for(int n = 1; n <= 10000; n++)
	{
		for(int i = 0; i < numElement; i++)
		{
			sampleArray[i] += n;
		}
		qDebug().noquote() << n << ",";
	}

	int result = sampleArray[0];

	delete [] sampleArray;

	return result;
}
