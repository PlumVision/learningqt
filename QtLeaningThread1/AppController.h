#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include <QObject>
#include "MainWindow.h"

//前方参照
class ProcessThread;
class ProgressDialog;
class CancelingDialog;

class AppController : public QObject
{
	Q_OBJECT

public:
	explicit AppController(QObject *parent = nullptr);//!<コンストラクタ
	~AppController();//!<デストラクタ

signals:
	void sigQuit();

public slots:

private slots:
	void slotReceiveStart();
	void slotRecieveThreadResult(int result);
	void slotReceiveThreadCanceled();
	void slotReceiveThreadAborting();

private:
	MainWindow* m_pclMainWindow;
	ProcessThread* m_pclThread;
	ProgressDialog* m_pclProgressDlg;	//!<処理中の表示を行うクラスへのポインタ
	CancelingDialog* m_pclCancelingDlg;	//!<処理キャンセル中の表示を行うクラスへのポインタ

	void createObjects();
};

#endif // APPCONTROLLER_H
