#include "AppController.h"
#include "ProcessThread.h"
#include "ProgressDialog.h"
#include "CancelingDialog.h"
#include <QDebug>


AppController::AppController(QObject *parent) : QObject(parent),
	m_pclMainWindow(nullptr), m_pclThread(nullptr), m_pclCancelingDlg(nullptr),
	m_pclProgressDlg(nullptr)
{
	createObjects();
}

AppController::~AppController()
{
	if(nullptr != m_pclCancelingDlg)
	{
		delete m_pclCancelingDlg;
		m_pclCancelingDlg = nullptr;
	}

	if(nullptr != m_pclProgressDlg)
	{
		delete m_pclProgressDlg;
		m_pclProgressDlg = nullptr;
	}

	if(nullptr != m_pclThread)
	{
		delete m_pclThread;
		m_pclThread = nullptr;
	}

	if(nullptr != m_pclMainWindow)
	{
		delete m_pclMainWindow;
		m_pclMainWindow = nullptr;
	}
}

void AppController::createObjects()
{
	m_pclMainWindow = new MainWindow();
	m_pclMainWindow->show();

	QObject::connect(m_pclMainWindow, SIGNAL(sigQuit()),
					 this, SIGNAL(sigQuit()));
	QObject::connect(m_pclMainWindow, SIGNAL(sigStart()),
					 this, SLOT(slotReceiveStart()));
}

void AppController::slotReceiveStart()
{
	if(nullptr != m_pclThread && m_pclThread->isRunning())
		return ;

	if(nullptr != m_pclThread)
	{
		m_pclThread->terminate();
		delete m_pclThread;
		m_pclThread = nullptr;
	}

	m_pclThread = new ProcessThread();

	QObject::connect(m_pclThread, SIGNAL(sigResult(int)),
					 this, SLOT(slotRecieveThreadResult(int)));
	QObject::connect(m_pclThread, SIGNAL(aborting()),
					 this, SLOT(slotReceiveThreadAborting()));
	QObject::connect(m_pclThread, SIGNAL(finished()),
					 this, SLOT(slotReceiveThreadCanceled()));

	if(nullptr != m_pclProgressDlg)
	{
		delete m_pclProgressDlg;
		m_pclProgressDlg = nullptr;
	}

	m_pclProgressDlg = new ProgressDialog();
	m_pclProgressDlg->setLabelText((tr("Excecuting sampleProcess1")));
	m_pclProgressDlg->setRange(0, 0);	//marquee表示設定;
	QObject::connect(m_pclProgressDlg, SIGNAL(canceled()),
					 m_pclThread, SLOT(abort()));

	m_pclThread->calc();
	m_pclProgressDlg->exec();

	return;

}

void AppController::slotRecieveThreadResult(int result)
{
	qDebug() << "AppController receieved result from a thread.";
	qDebug() << "result =" << result;
}

void AppController::slotReceiveThreadCanceled()
{

	if(nullptr != m_pclCancelingDlg)
	{
		m_pclCancelingDlg->close();
		delete m_pclCancelingDlg;
		m_pclCancelingDlg = nullptr;
	}

	if(nullptr != m_pclThread)
	{
		delete m_pclThread;
		m_pclThread = nullptr;
	}
}

void AppController::slotReceiveThreadAborting()
{
	if(nullptr != m_pclCancelingDlg)
	{
		delete m_pclCancelingDlg;
		m_pclCancelingDlg = nullptr;
	}
	m_pclCancelingDlg = new CancelingDialog(m_pclMainWindow);
	m_pclCancelingDlg->setWindowFlags(Qt::Window
									  | Qt::WindowTitleHint
									  | Qt::CustomizeWindowHint);
	m_pclCancelingDlg->exec();
}
