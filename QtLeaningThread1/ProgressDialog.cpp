#include "ProgressDialog.h"

ProgressDialog::ProgressDialog()
{
	setWindowFlags(Qt::Window
				   | Qt::WindowTitleHint
				   | Qt::CustomizeWindowHint);
	setWindowTitle("Processing...");
}
