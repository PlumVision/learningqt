#include "ProcessThread.h"
#include "ProcessFunctions.h"
#include <QDebug>

ProcessThread::ProcessThread():
	m_bStopped(true)
{

}

ProcessThread::~ProcessThread()
{

}

void ProcessThread::calc()
{
	if(!isRunning())
		start();
}

void ProcessThread::abort()
{
	m_clMutex.lock();
	m_bStopped = true;
	m_clMutex.unlock();

	qDebug() << "thread aborting";
	emit aborting();
}

void ProcessThread::run()
{
	m_clMutex.lock();
		m_bStopped = false;
	m_clMutex.unlock();

	int ret = sampleProcess1();
	sigResult(ret);

	m_clMutex.lock();
		m_bStopped = true;
	m_clMutex.unlock();

	return;
}
