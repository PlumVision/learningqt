#ifndef PROCESSTHREAD_H
#define PROCESSTHREAD_H

#include<QThread>
#include<QMutex>

class ProcessThread : public QThread
{
	Q_OBJECT

public:
	ProcessThread();
	~ProcessThread();

signals:
	void aborting();
	void sigResult(int value);

public slots:
	void calc();
	void abort();

protected:
	void run();

private:
	//コピーさせない
	ProcessThread(const ProcessThread&);
	ProcessThread& operator = (const ProcessThread&);

	bool m_bStopped;	//!<中断を促すフラグ
	QMutex m_clMutex;	//!<ミューテックス
};

#endif // PROCESSTHREAD_H
