#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QProgressDialog>

class ProgressDialog : public QProgressDialog
{

	Q_OBJECT

public:
	ProgressDialog();
};

#endif // PROGRESSDIALOG_H
