//#include "MainWindow.h"
#include "AppController.h"
#include <QApplication>


#ifdef _DEBUG
	#include <vld.h>
#endif

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	//MainWindow w;
	//w.show();
	AppController a;
	QObject::connect(&a, SIGNAL(sigQuit()),
					 &app, SLOT(quit()));

	return app.exec();
}
