//--------------------------------------------------------------------------------//
/*!
	@file	GraphicsViewRevised1.h
	@brief	QGraphicsViewを継承し、マウスドラッグ操作でトリミング情報を発生する拡張をしたクラスのヘッダ
	@author	梅村
	@note	新：GrahpicsViewWithTrimmerの仕様では目的に合わないところが出てきたのでQGraphicsViewを継承して改造。
	@note	（旧：テーマまたぎの共有リポジトリREX_Commonに公開したGraphicsViewWithTrimmerを継承）
	@note	※QGraphicsViewの標準ではDrag&Dropはできないので以下の記事を参考に改造
	@note	https://sites.google.com/site/qoopazero/home/qt/graphicsview_dropevent
	@note	現状Drop対象のファイルはcbiに限定している。
	@note	【QtPaintAnalyzerPrototype1】プロジェクトで定義したCbiViewerで使用
	@note	汎用ではないので共有リポジトリへの公開はしない予定。
	@note	ファイルの取り扱いを簡単にしたいので.cppを持たず.h内に処理コードも記述
	@note	追記：【QtPaintAnalyzerPrototype1】プロジェクトの[XYStageDemoViewer]からも使いたいのでさらに改造。
*/
//--------------------------------------------------------------------------------//
#ifndef GRAPHICSVIEWREVISED1_H
#define GRAPHICSVIEWREVISED1_H

#include <QGraphicsView>	//#include "GraphicsViewWithTrimmer.h"
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QDebug>
#include <QScrollBar>
#include <QMouseEvent>

class GraphicsViewRevised1 : public QGraphicsView	//GraphicsViewWithTrimmer
{
	Q_OBJECT
public:
	explicit GraphicsViewRevised1(QWidget* parent = 0)
		//: GraphicsViewWithTrimmer(parent){
		: QGraphicsView(parent),
		m_clDragRect(QRect()),
		m_iMousePressButton(Qt::LeftButton)
	{
		//初期状態の背景等を設定（空のScene等を表示する場合）
		QPalette palette;
		QBrush brush(QColor(0, 0, 0, 255));
		brush.setStyle(Qt::Dense4Pattern);
		palette.setBrush(QPalette::Active, QPalette::Base, brush);
		palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
		QBrush brush1(QColor(240, 240, 240, 255));
		brush1.setStyle(Qt::SolidPattern);
		palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
		this->setPalette(palette);

		//スクロールバーの値変化を捕まえて発信する仕掛けをつくる。
		QScrollBar* pVBar = this->verticalScrollBar();
		QScrollBar* pHBar = this->horizontalScrollBar();

		QObject::connect(pVBar,SIGNAL(valueChanged(int)),
						 this, SIGNAL(sigVScrollBarValueChanged(int)));
		QObject::connect(pHBar,SIGNAL(valueChanged(int)),
						 this, SIGNAL(sigHScrollBarValueChanged(int)));

	}		//!<コンストラクタ

signals:
	void sigRequestOpenCbi(QString filepath);	//!<CBIファイルのオープン要求を発信
	void sigVScrollBarValueChanged(int);		//!<垂直スクロールバー値変化を発信
	void sigHScrollBarValueChanged(int);		//!<水平スクロールバー値変化を発信
	void sigMousePress(QPoint, int);			//!<マウスボタン押下時のGrahicsView上の座標とボタン種を発信
	void sigMouseMoveRect(QRect, int);			//!<マウスドラッグ時の開始点と現在点のなすGraphicsView上のRectを発信
	void sigMouseMove(QPoint, int);				//!<マウスドラッグ時の座標とボタン種を発信
	void sigMouseDoublClick(QPoint, int);		//!<マウスボタンダブルクリック時のGraphicsView上の座標とボタン種を発信
	void sigMouseRelease(QPoint, int);			//!<マウスボタンを離したときの座標とボタン種を発信

protected:
	//Drag&Drop系の再実装
	void dragEnterEvent(QDragEnterEvent *event) override
	{
		qDebug() << "GraphicsViewRevise1::dragEnterEvent";
		event->acceptProposedAction();

		//デフォ処理
		QGraphicsView::dragEnterEvent(event);
	}
	void dragMoveEvent(QDragMoveEvent *event) override {Q_UNUSED(event);}
	void dragLeaveEvent(QDragLeaveEvent *event) override {Q_UNUSED(event);}
	void dropEvent(QDropEvent *event) override
	{
		qDebug() << "GraphicsViewRevise1::dropEnterEvent";

		//Dropされたファイルの拡張子を調べる
		int count = 0;
		QString filepathMatched;
		foreach (QUrl url, event->mimeData()->urls())
		{
			QFileInfo info(url.toLocalFile());
			QString suffix = info.suffix();
			if("cbi" == suffix)
			{
				filepathMatched = info.absoluteFilePath();
				count++;
			}
		}
		//対象のファイルが1つだけだったら、それを開く
		if(1 == count)
		{
			qDebug() << "1 Cbi file dropped.";
			emit sigRequestOpenCbi(filepathMatched);
		}
		else	//対象が複数あったり、１つもない場合は何もしない。
			qDebug() << "no suitable file droped.";

		//デフォ処理
		QGraphicsView::dropEvent(event);
	}
	//Mouseイベント系の再実装
	void mousePressEvent(QMouseEvent *event) override
	{
		m_iMousePressButton = event->button();
		QPoint pt = event->pos();
		emit sigMousePress(pt, m_iMousePressButton);
		m_clDragRect.setX(pt.x());
		m_clDragRect.setY(pt.y());

		//デフォ処理
		QGraphicsView::mousePressEvent(event);
	}
	void mouseMoveEvent(QMouseEvent *event) override
	{
		QPoint pt = event->pos();
		m_clDragRect.setWidth(pt.x() - m_clDragRect.x());
		m_clDragRect.setHeight(pt.y() - m_clDragRect.y());

		emit sigMouseMove(pt, m_iMousePressButton);

		//Move開始点と現在点からなるRectとMove開始時のpressボタン種を発信
		emit sigMouseMoveRect(m_clDragRect, m_iMousePressButton);

		//デフォ処理
		QGraphicsView::mouseMoveEvent(event);
	}
	void mouseReleaseEvent(QMouseEvent *event) override
	{
		emit sigMouseRelease(event->pos(), event->button());
		//デフォ処理
		QGraphicsView::mouseReleaseEvent(event);
	}
	void mouseDoubleClickEvent(QMouseEvent *event) override
	{
		QPoint pt = event->pos();
		emit sigMouseDoublClick(pt, event->button());

		//デフォ処理
		QGraphicsView::mouseDoubleClickEvent(event);
	}

private:
	QRect m_clDragRect;		//マウスドラッグ時に開始点と現在点からなる矩形座標保持用
	int m_iMousePressButton;//マウスボタン押下時のボタン種記憶用

};

#endif // GRAPHICSVIEWREVISED1_H
