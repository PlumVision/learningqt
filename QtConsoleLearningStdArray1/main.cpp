#include <QCoreApplication>
#include <QDebug>
#include <array>

#ifdef Q_OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	try
	{
	#if 0
		int sz = 10; //動的に要素数を決めることは不可
	#else
		#if 1
			const int sz = 10;	//constならOKだが、実質決め打ち
		#else
			const int sz = 1280 * 1024;	//stack overflow
		#endif
	#endif

		std::array <int, sz> arr;

		qDebug() << "size of arary =" << arr.size();

		//for(int i = 0; i < arr.size() + 1; i++)//例外をわざと起こす
		//for(int i = -1; i < arr.size(); i++) //例外おきない→おかしな値になる。
		for(int i = 0; i < arr.size(); i++)
		{
			//arr[i] = i; //例外起きない
			arr.at(i) = i;	//例外起きる
		}

		qDebug() << "array.at(i)";
		//for(int i = 0; i < arr.size() + 1; i++)//例外をわざと起こす
		//for(int i = -1; i < arr.size(); i++) //ん？例外おきない？
		for(int i = 0; i < arr.size(); i++)
		{
			//qDebug() << arr[i];//例外起きない
			qDebug() << arr.at(i);//例外起きる
		}

		qDebug() << "for(auto itr = arr.begin();...";
		for(auto itr = arr.begin(); itr != arr.end(); ++itr)
		{
			qDebug() << *itr;
		}

		qDebug() << "for(auto elm: arr";
		for(auto elm: arr)
		{
			qDebug() << elm;
		}
	}
	catch(std::bad_alloc& e)
	{
		// メモリ確保に失敗
		qDebug() << e.what();
	}
	catch(std::out_of_range& e)
	{
		qDebug() << e.what();
	}
	catch(...)
	{
		qDebug() << "unexpected exception thrown.";
	}

	return 0;//a.exec();
}
