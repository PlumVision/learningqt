TEMPLATE      = subdirs
SUBDIRS       = chap01/age \
                chap01/hello \
                chap01/quit \
                chap02/find \
                chap02/gotocell1 \
                chap02/gotocell2 \
                chap02/sort \
                chap03/spreadsheet \
                chap04/spreadsheet \
                chap05/hexspinbox \
                chap05/iconeditor \
                chap05/iconeditorplugin \
                chap05/plotter \
                chap06/findfile1 \
                chap06/findfile2 \
                chap06/findfile3 \
                chap06/mailclient \
                chap06/mdieditor \
                chap06/preferences \
                chap06/splitter \
                chap07/ticker \
                chap08/oventimer \
                chap08/tetrahedron \
                chap09/projectchooser \
                chap10/cities \
                chap10/colornames \
                chap10/coordinatesetter \
                chap10/currencies \
                chap10/directoryviewer \
                chap10/flowchartsymbolpicker \
                chap10/regexpparser \
                chap10/settingsviewer \
                chap10/teamleaders \
                chap10/trackeditor \
                chap12/imageconverter \
                chap12/imagespace \
                chap12/tidy \
                chap13/cdcollection \
                chap14/ftpget \
                chap14/httpget \
                chap14/spider \
                chap14/tripplanner \
                chap14/tripserver \
                chap14/weatherballoon \
                chap14/weatherstation \
                chap15/domparser \
                chap15/saxhandler \
                chap18/imagepro \
                chap18/semaphores \
                chap18/threads \
                chap18/waitconditions \
                chap19/basiceffectsplugin \
                chap19/cursorplugin \
                chap19/extraeffectsplugin \
                chap19/textart \
                chap20/tictactoe
win32 {
SUBDIRS      += chap20/addressbook \
                chap20/bouncer \
                chap20/mediaplayer
}
