greaterThan(QT_MAJOR_VERSION, 4): message("Use mdieditor-2nd-edition instead")

TEMPLATE      = app
HEADERS       = editor.h \
                mainwindow.h
SOURCES       = editor.cpp \
                main.cpp \
                mainwindow.cpp
RESOURCES     = mdieditor.qrc
