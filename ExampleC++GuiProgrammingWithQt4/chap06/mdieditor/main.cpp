#include <QApplication>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QStringList args = app.arguments();

    MainWindow mainWin;
    if (args.count() > 1) {
        for (int i = 1; i < args.count(); ++i)
            mainWin.openFile(args[i]);
    } else {
        mainWin.newFile();
    }

    mainWin.show();
    return app.exec();
}
