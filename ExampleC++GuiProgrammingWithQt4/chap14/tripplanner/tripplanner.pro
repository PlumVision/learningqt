greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TEMPLATE      = app
QT           += network
HEADERS       = tripplanner.h
SOURCES       = main.cpp \
                tripplanner.cpp
FORMS         = tripplanner.ui
