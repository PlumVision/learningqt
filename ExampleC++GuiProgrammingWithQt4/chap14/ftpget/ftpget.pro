TEMPLATE      = app
QT            = core network
CONFIG       += console
CONFIG       -= app_bundle
HEADERS       = ftpget.h
SOURCES       = ftpget.cpp \
                main.cpp
greaterThan(QT_MAJOR_VERSION, 4): {
    INCLUDEPATH += $$[QT_INSTALL_HEADERS]
    LIBS += -L$$[QT_INSTALL_LIBS] -lQt5Ftp
}
