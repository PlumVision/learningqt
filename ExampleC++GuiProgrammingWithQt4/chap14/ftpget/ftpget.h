#ifndef FTPGET_H
#define FTPGET_H

#include <QFile>
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <QtFtp/QtFtp>
#else
#include <QFtp>
#endif

class QUrl;

class FtpGet : public QObject
{
    Q_OBJECT

public:
    FtpGet(QObject *parent = 0);

    bool getFile(const QUrl &url);

signals:
    void done();

private slots:
    void ftpDone(bool error);

private:
    QFtp ftp;
    QFile file;
};

#endif
