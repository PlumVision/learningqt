#include <QtCore>
#include <iostream>

#include "ftpget.h"

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QStringList args = app.arguments();

    if (args.count() != 2) {
        cerr << "Usage: ftpget url" << endl
             << "Example:" << endl
             << "    ftpget ftp://ftp.trolltech.com/mirrors" << endl;
        return 1;
    }

    FtpGet getter;
    if (!getter.getFile(QUrl(args[1])))
        return 1;

    QObject::connect(&getter, SIGNAL(done()), &app, SLOT(quit()));

    return app.exec();
}
