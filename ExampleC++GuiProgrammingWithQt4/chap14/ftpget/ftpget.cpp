#include <QtCore>
#include <QtNetwork>
#include <iostream>

#include "ftpget.h"

using namespace std;

FtpGet::FtpGet(QObject *parent)
    : QObject(parent)
{
    connect(&ftp, SIGNAL(done(bool)), this, SLOT(ftpDone(bool)));
}

bool FtpGet::getFile(const QUrl &url)
{
    if (!url.isValid()) {
        cerr << "Error: Invalid URL" << endl;
        return false;
    }

    if (url.scheme() != "ftp") {
        cerr << "Error: URL must start with 'ftp:'" << endl;
        return false;
    }

    if (url.path().isEmpty()) {
        cerr << "Error: URL has no path" << endl;
        return false;
    }

    QString localFileName = QFileInfo(url.path()).fileName();
    if (localFileName.isEmpty())
        localFileName = "ftpget.out";

    file.setFileName(localFileName);
    if (!file.open(QIODevice::WriteOnly)) {
        cerr << "Error: Cannot open " << qPrintable(file.fileName())
             << " for writing: " << qPrintable(file.errorString())
             << endl;
        return false;
    }

    ftp.connectToHost(url.host(), url.port(21));
    ftp.login();
    ftp.get(url.path(), &file);
    ftp.close();
    return true;
}

void FtpGet::ftpDone(bool error)
{
    if (error) {
        cerr << "Error: " << qPrintable(ftp.errorString()) << endl;
    } else {
        cerr << "File downloaded as " << qPrintable(file.fileName())
             << endl;
    }
    file.close();
    emit done();
}
