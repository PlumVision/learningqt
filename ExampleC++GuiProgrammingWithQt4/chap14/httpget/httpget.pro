TEMPLATE      = app
QT            = core network
CONFIG       += console
CONFIG       -= app_bundle
HEADERS       = httpget.h
SOURCES       = httpget.cpp \
                main.cpp
greaterThan(QT_MAJOR_VERSION, 4): {
    INCLUDEPATH += $$[QT_INSTALL_HEADERS]
    LIBS += -L$$[QT_INSTALL_LIBS] -lQt5Http
}
