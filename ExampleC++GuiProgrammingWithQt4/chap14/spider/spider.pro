TEMPLATE      = app
QT            = core network
CONFIG       += console
CONFIG       -= app_bundle
HEADERS       = spider.h
SOURCES       = main.cpp \
                spider.cpp
greaterThan(QT_MAJOR_VERSION, 4): {
    INCLUDEPATH += $$[QT_INSTALL_HEADERS]
    LIBS += -L$$[QT_INSTALL_LIBS] -lQt5Ftp
}
