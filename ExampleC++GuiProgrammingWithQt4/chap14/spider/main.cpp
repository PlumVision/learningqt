#include <QtCore>
#include <iostream>

#include "spider.h"

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QStringList args = app.arguments();

    if (args.count() != 2) {
        cerr << "Usage: spider url" << endl
             << "Example:" << endl
             << "    spider ftp://ftp.trolltech.com/freebies/leafnode"
             << endl;
        return 1;
    }

    Spider spider;
    if (!spider.getDirectory(QUrl(args[1])))
        return 1;

    QObject::connect(&spider, SIGNAL(done()), &app, SLOT(quit()));

    return app.exec();
}
