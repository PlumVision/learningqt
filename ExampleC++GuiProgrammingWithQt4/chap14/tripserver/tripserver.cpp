#include "clientsocket.h"
#include "tripserver.h"

TripServer::TripServer(QObject *parent)
    : QTcpServer(parent)
{
}

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
void TripServer::incomingConnection(qintptr socketId)
#else
void TripServer::incomingConnection(int socketId)
#endif
{
    ClientSocket *socket = new ClientSocket(this);
    socket->setSocketDescriptor(socketId);
}
