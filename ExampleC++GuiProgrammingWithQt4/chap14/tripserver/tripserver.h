#ifndef TRIPSERVER_H
#define TRIPSERVER_H

#include <QTcpServer>

class TripServer : public QTcpServer
{
    Q_OBJECT

public:
    TripServer(QObject *parent = 0);

private:
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    void incomingConnection(qintptr socketId);
#else
    void incomingConnection(int socketId);
#endif
};

#endif
