greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TEMPLATE      = app
QT           += network
CONFIG       += console
HEADERS       = clientsocket.h \
                tripserver.h
SOURCES       = clientsocket.cpp \
                main.cpp \
                tripserver.cpp
