#include <QtGui>

#include "cursorhandler.h"
#include "cursorplugin.h"

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
QStringList CursorPlugin::keys() const
{
    return QStringList() << "cur";
}
#endif

QImageIOPlugin::Capabilities
CursorPlugin::capabilities(QIODevice *device,
                           const QByteArray &format) const
{
    if (format == "cur")
        return CanRead;

    if (format.isEmpty()) {
        CursorHandler handler;
        handler.setDevice(device);
        if (handler.canRead())
            return CanRead;
    }

    return 0;
}

QImageIOHandler *CursorPlugin::create(QIODevice *device,
                                      const QByteArray &format) const
{
    CursorHandler *handler = new CursorHandler;
    handler->setDevice(device);
    handler->setFormat(format);
    return handler;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
Q_EXPORT_PLUGIN2(cursorplugin, CursorPlugin)
#endif
