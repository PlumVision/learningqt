#ifndef CURSORPLUGIN_H
#define CURSORPLUGIN_H

#include <QImageIOPlugin>

class CursorPlugin : public QImageIOPlugin
{
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    Q_PLUGIN_METADATA(IID "com.software-inc.TextArt.QImageIOHandlerFactoryInterface" FILE "cursor.json")
#endif

public:
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QStringList keys() const;
#endif
    Capabilities capabilities(QIODevice *device,
                              const QByteArray &format) const;
    QImageIOHandler *create(QIODevice *device,
                            const QByteArray &format) const;
};

#endif
