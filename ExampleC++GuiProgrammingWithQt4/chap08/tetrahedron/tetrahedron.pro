greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE      = app
QT           += opengl
CONFIG       += console
HEADERS       = tetrahedron.h
SOURCES       = main.cpp \
                tetrahedron.cpp

unix:!macx:LIBS += -lGLU
