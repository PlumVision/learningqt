#ifndef REGEXPPARSER_H
#define REGEXPPARSER_H

#include "node.h"

class RegExpParser
{
public:
    Node *parse(const QString &regExp);

private:
    Node *parseRegExp();
    Node *parseExpression();
    Node *parseTerm();
    Node *parseFactor();
    Node *parseAtom();
    void addChild(Node *parent, Node *child);
    bool addTerminal(Node *parent, const QString &str);

    QString in;
    int pos;
};

#endif
