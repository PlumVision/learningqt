#ifndef NODE_H
#define NODE_H

#include <QList>
#include <QString>

class Node
{
public:
    enum Type { RegExp, Expression, Term, Factor, Atom, Terminal };

    Node(Type type, const QString &str = "");
    ~Node();

    Type type;
    QString str;
    Node *parent;
    QList<Node *> children;
};

#endif
