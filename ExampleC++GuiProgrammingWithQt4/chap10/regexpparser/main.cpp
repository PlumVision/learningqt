#include <QApplication>

#include "regexpwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    RegExpWindow window;
    window.show();
    return app.exec();
}
