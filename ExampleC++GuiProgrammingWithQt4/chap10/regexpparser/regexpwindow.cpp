#include <QtGui>
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <QtWidgets>
#endif

#include "regexpmodel.h"
#include "regexpparser.h"
#include "regexpwindow.h"

RegExpWindow::RegExpWindow()
{
    label = new QLabel(tr("Regular expression:"));
    lineEdit = new QLineEdit;

    regExpModel = new RegExpModel(this);

    treeView = new QTreeView;
    treeView->setModel(regExpModel);

    connect(lineEdit, SIGNAL(textChanged(const QString &)),
            this, SLOT(regExpChanged(const QString &)));

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(label, 0, 0);
    layout->addWidget(lineEdit, 0, 1);
    layout->addWidget(treeView, 1, 0, 1, 2);
    setLayout(layout);

    setWindowTitle(tr("Regexp Parser"));
}

void RegExpWindow::regExpChanged(const QString &regExp)
{
    RegExpParser parser;
    Node *rootNode = parser.parse(regExp);
    regExpModel->setRootNode(rootNode);
}
