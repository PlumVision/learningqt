#include <QtCore>

#include "node.h"
#include "regexpparser.h"

Node *RegExpParser::parse(const QString &regExp)
{
    in = regExp;
    pos = 0;
    return parseRegExp();
}

Node *RegExpParser::parseRegExp()
{
    Node *node = new Node(Node::RegExp);
    addChild(node, parseExpression());
    return node;
}

Node *RegExpParser::parseExpression()
{
    Node *node = new Node(Node::Expression);
    addChild(node, parseTerm());
    while (addTerminal(node, "|"))
        addChild(node, parseTerm());
    return node;
}

Node *RegExpParser::parseTerm()
{
    Node *node = new Node(Node::Term);
    while (pos < in.length() && in[pos] != '|' && in[pos] != ')')
        addChild(node, parseFactor());
    return node;
}

Node *RegExpParser::parseFactor()
{
    Node *node = new Node(Node::Factor);
    addChild(node, parseAtom());
    if (!addTerminal(node, "?")) {
        if (!addTerminal(node, "*"))
            addTerminal(node, "+");
    }
    return node;
}

Node *RegExpParser::parseAtom()
{
    Node *node = new Node(Node::Atom);
    if (addTerminal(node, "(")) {
        addChild(node, parseExpression());
        addTerminal(node, ")");
    } else {
        QChar ch = in[pos];
        if (ch == '\\') {
            addTerminal(node, in.mid(pos, 2));
        } else {
            addTerminal(node, QString(ch));
        }
    }
    return node;
}

void RegExpParser::addChild(Node *parent, Node *child)
{
    parent->children += child;
    parent->str += child->str;
    child->parent = parent;
}

bool RegExpParser::addTerminal(Node *parent, const QString &str)
{
    if (in.mid(pos, str.length()) == str) {
        addChild(parent, new Node(Node::Terminal, str));
        pos += str.length();
        return true;
    }
    return false;
}
