#ifndef REGEXPWINDOW_H
#define REGEXPWINDOW_H

#include <QWidget>

class QLabel;
class QLineEdit;
class QTreeView;
class RegExpModel;

class RegExpWindow : public QWidget
{
    Q_OBJECT

public:
    RegExpWindow();

private slots:
    void regExpChanged(const QString &regExp);

private:
    QLabel *label;
    QLineEdit *lineEdit;
    RegExpModel *regExpModel;
    QTreeView *treeView;
};

#endif
