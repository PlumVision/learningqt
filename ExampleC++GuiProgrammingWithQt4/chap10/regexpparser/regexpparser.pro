greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TEMPLATE      = app
HEADERS       = node.h \
                regexpmodel.h \
                regexpparser.h \
                regexpwindow.h
SOURCES       = main.cpp \
                node.cpp \
                regexpmodel.cpp \
                regexpparser.cpp \
                regexpwindow.cpp
