#ifndef MAINFORM_H
#define MAINFORM_H

#include <QDialog>

class QModelIndex;
class QPushButton;
class QSplitter;
class QSqlRecord;
class QSqlRelationalTableModel;
class QSqlTableModel;
class QTableView;

class MainForm : public QWidget
{
    Q_OBJECT

public:
    MainForm();

private slots:
    void addCd();
    void deleteCd();
    void addTrack();
    void deleteTrack();
    void editArtists();
    void currentCdChanged(const QModelIndex &index);
    void beforeInsertCd(QSqlRecord &record);
    void beforeInsertTrack(QSqlRecord &record);
    void refreshTrackViewHeader();

private:
    enum {
        Cd_Id = 0,
        Cd_Title = 1,
        Cd_ArtistId = 2,
        Cd_Year = 3
    };

    enum {
        Track_Id = 0,
        Track_Title = 1,
        Track_Duration = 2,
        Track_CdId = 3
    };

    QSqlRelationalTableModel *cdModel;
    QSqlTableModel *trackModel;
    QTableView *cdTableView;
    QTableView *trackTableView;
    QPushButton *addCdButton;
    QPushButton *deleteCdButton;
    QPushButton *addTrackButton;
    QPushButton *deleteTrackButton;
    QPushButton *editArtistsButton;
    QPushButton *quitButton;
};

#endif
