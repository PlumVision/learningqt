#include <QtGui>
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <QtWidgets>
#endif
#include <QtSql>

#include "artistform.h"
#include "generateid.h"
#include "mainform.h"
#include "trackdelegate.h"

MainForm::MainForm()
{
    cdModel = new QSqlRelationalTableModel(this);
    cdModel->setTable("cd");
    cdModel->setRelation(Cd_ArtistId,
                         QSqlRelation("artist", "id", "name"));
    cdModel->setSort(Cd_Title, Qt::AscendingOrder);
    cdModel->setHeaderData(Cd_Title, Qt::Horizontal, tr("Title"));
    cdModel->setHeaderData(Cd_ArtistId, Qt::Horizontal, tr("Artist"));
    cdModel->setHeaderData(Cd_Year, Qt::Horizontal, tr("Year"));
    cdModel->select();

    cdTableView = new QTableView;
    cdTableView->setModel(cdModel);
    cdTableView->setItemDelegate(new QSqlRelationalDelegate(this));
    cdTableView->setSelectionMode(QAbstractItemView::SingleSelection);
    cdTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    cdTableView->setColumnHidden(Cd_Id, true);
    cdTableView->resizeColumnsToContents();

    trackModel = new QSqlTableModel(this);
    trackModel->setTable("track");
    trackModel->setHeaderData(Track_Title, Qt::Horizontal, tr("Title"));
    trackModel->setHeaderData(Track_Duration, Qt::Horizontal,
                              tr("Duration"));

    trackTableView = new QTableView;
    trackTableView->setModel(trackModel);
    trackTableView->setItemDelegate(
            new TrackDelegate(Track_Duration, this));
    trackTableView->setSelectionMode(
            QAbstractItemView::SingleSelection);
    trackTableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    addCdButton = new QPushButton(tr("Add &CD"));
    deleteCdButton = new QPushButton(tr("&Delete CD"));
    addTrackButton = new QPushButton(tr("Add &Track"));
    deleteTrackButton = new QPushButton(tr("D&elete Track"));
    editArtistsButton = new QPushButton(tr("Edit &Artists..."));
    quitButton = new QPushButton(tr("&Quit"));

    addCdButton->setFocusPolicy(Qt::NoFocus);
    deleteCdButton->setFocusPolicy(Qt::NoFocus);
    addTrackButton->setFocusPolicy(Qt::NoFocus);
    deleteTrackButton->setFocusPolicy(Qt::NoFocus);
    editArtistsButton->setFocusPolicy(Qt::NoFocus);

    connect(cdTableView->selectionModel(),
            SIGNAL(currentRowChanged(const QModelIndex &,
                                     const QModelIndex &)),
            this, SLOT(currentCdChanged(const QModelIndex &)));
    connect(cdModel, SIGNAL(beforeInsert(QSqlRecord &)),
            this, SLOT(beforeInsertCd(QSqlRecord &)));
    connect(trackModel, SIGNAL(beforeInsert(QSqlRecord &)),
            this, SLOT(beforeInsertTrack(QSqlRecord &)));
    connect(trackModel, SIGNAL(rowsInserted(const QModelIndex &, int,
                                            int)),
            this, SLOT(refreshTrackViewHeader()));
    connect(addCdButton, SIGNAL(clicked()), this, SLOT(addCd()));
    connect(deleteCdButton, SIGNAL(clicked()), this, SLOT(deleteCd()));
    connect(addTrackButton, SIGNAL(clicked()), this, SLOT(addTrack()));
    connect(deleteTrackButton, SIGNAL(clicked()),
            this, SLOT(deleteTrack()));
    connect(editArtistsButton, SIGNAL(clicked()),
            this, SLOT(editArtists()));
    connect(quitButton, SIGNAL(clicked()), this, SLOT(close()));

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(addCdButton);
    buttonLayout->addWidget(deleteCdButton);
    buttonLayout->addWidget(addTrackButton);
    buttonLayout->addWidget(deleteTrackButton);
    buttonLayout->addWidget(editArtistsButton);
    buttonLayout->addStretch();
    buttonLayout->addWidget(quitButton);

    QSplitter *splitter = new QSplitter(Qt::Vertical);
    splitter->addWidget(cdTableView);
    splitter->addWidget(trackTableView);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(splitter);
    mainLayout->addLayout(buttonLayout);
    setLayout(mainLayout);

    setWindowTitle(tr("CD Collection"));
    currentCdChanged(QModelIndex());
}

void MainForm::addCd()
{
    int row = 0;
    if (cdTableView->currentIndex().isValid())
        row = cdTableView->currentIndex().row();

    cdModel->insertRow(row);
    cdModel->setData(cdModel->index(row, Cd_Year),
                     QDate::currentDate().year());

    QModelIndex index = cdModel->index(row, Cd_Title);
    cdTableView->setCurrentIndex(index);
    cdTableView->edit(index);
}

void MainForm::deleteCd()
{
    QModelIndex index = cdTableView->currentIndex();
    if (!index.isValid())
        return;
    QSqlDatabase db = QSqlDatabase::database();
    db.transaction();
    QSqlRecord record = cdModel->record(index.row());
    int id = record.value(Cd_Id).toInt();
    int tracks = 0;
    QSqlQuery query;
    query.exec(QString("SELECT COUNT(*) FROM track WHERE cdid = %1")
               .arg(id));
    if (query.next())
        tracks = query.value(0).toInt();
    if (tracks > 0) {
        int r = QMessageBox::question(this, tr("Delete CD"),
                        tr("Delete \"%1\" and all its tracks?")
                        .arg(record.value(Cd_ArtistId).toString()),
                        QMessageBox::Yes | QMessageBox::Default,
                        QMessageBox::No | QMessageBox::Escape);
        if (r == QMessageBox::No) {
            db.rollback();
            return;
        }
        query.exec(QString("DELETE FROM track WHERE cdid = %1")
                   .arg(id));
    }
    cdModel->removeRow(index.row());
    cdModel->submitAll();
    db.commit();

    currentCdChanged(QModelIndex());
}

void MainForm::addTrack()
{
    if (!cdTableView->currentIndex().isValid())
        return;

    int row = 0;
    if (trackTableView->currentIndex().isValid())
        row = trackTableView->currentIndex().row();

    trackModel->insertRow(row);
    QModelIndex index = trackModel->index(row, Track_Title);
    trackTableView->setCurrentIndex(index);
    trackTableView->edit(index);
}

void MainForm::deleteTrack()
{
    trackModel->removeRow(trackTableView->currentIndex().row());
    if (trackModel->rowCount() == 0)
        trackTableView->horizontalHeader()->setVisible(false);
}

void MainForm::editArtists()
{
    QSqlRecord record = cdModel->record(cdTableView->currentIndex()
                                        .row());
    ArtistForm artistForm(record.value(Cd_ArtistId).toString(), this);
    artistForm.exec();
    cdModel->select();
}

void MainForm::currentCdChanged(const QModelIndex &index)
{
    if (index.isValid()) {
        QSqlRecord record = cdModel->record(index.row());
        int id = record.value("id").toInt();
        trackModel->setFilter(QString("cdid = %1").arg(id));
    } else {
        trackModel->setFilter("cdid = -1");
    }

    trackModel->select();
    refreshTrackViewHeader();
}

void MainForm::beforeInsertCd(QSqlRecord &record)
{
    record.setValue("id", generateId("cd"));
}

void MainForm::beforeInsertTrack(QSqlRecord &record)
{
    QSqlRecord cdRecord = cdModel->record(cdTableView->currentIndex()
                                          .row());
    record.setValue("id", generateId("track"));
    record.setValue("cdid", cdRecord.value(Cd_Id).toInt());
}

void MainForm::refreshTrackViewHeader()
{
    trackTableView->horizontalHeader()->setVisible(
            trackModel->rowCount() > 0);
    trackTableView->setColumnHidden(Track_Id, true);
    trackTableView->setColumnHidden(Track_CdId, true);
    trackTableView->resizeColumnsToContents();
}
