#include <QtGui>
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <QtWidgets>
#endif
#include <QtSql>

#include "connection.h"
#include "mainform.h"

#ifdef _OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    if (!createConnection())
        return 1;
    MainForm mainForm;
    mainForm.show();
    return app.exec();
}
