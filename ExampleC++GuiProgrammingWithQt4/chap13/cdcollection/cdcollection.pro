greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TEMPLATE      = app
QT           += sql
HEADERS       = artistform.h \
                connection.h \
                generateid.h \
                mainform.h \
                trackdelegate.h
SOURCES       = artistform.cpp \
                main.cpp \
                mainform.cpp \
                trackdelegate.cpp

win32:{
	CONFIG(debug, debug|release){

		#Visual Studio 向け
#		win32-msvc2013	{
		win32-msvc2015	{
			DEFINES += _DEBUG
			DEFINES += _OS_WIN
			INCLUDEPATH += $(VLDROOT)/include
			LIBS += -L$(VLDROOT)/lib/Win64 \
					-lvld
#			LIBS += -L$(QWTROOT)/x64/lib \
#					-lqwtd
		}
		#MinGW
#		win32-g++{
#			LIBS += -L$(QWTROOT)/minGW/lib \
#					-lqwtd
#			}
	}

}
