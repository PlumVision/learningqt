#ifndef ARTISTFORM_H
#define ARTISTFORM_H

#include <QDialog>

class QPushButton;
class QSqlRecord;
class QSqlTableModel;
class QTableView;

class ArtistForm : public QDialog
{
    Q_OBJECT

public:
    ArtistForm(const QString &name, QWidget *parent = 0);

private slots:
    void addArtist();
    void deleteArtist();
    void beforeInsertArtist(QSqlRecord &record);

private:
    enum {
        Artist_Id = 0,
        Artist_Name = 1,
        Artist_Country = 2
    };

    QSqlTableModel *model;
    QTableView *tableView;
    QPushButton *addButton;
    QPushButton *deleteButton;
    QPushButton *closeButton;
};

#endif
