#include <QCoreApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	auto plus = [](int a, int b){return a + b; };
	int result = plus(2, 3);

	qDebug() << "plus(2, 3) =" << result;
	return 0;// a.exec();
}
