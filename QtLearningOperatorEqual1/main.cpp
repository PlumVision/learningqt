#include <QCoreApplication>
#include <iostream>
#include <QDebug>
#include <memory>

#ifdef Q_OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif

//#define USE_UNIQUE_PTR

template<class T>
class Sample{
public:
	//デフォルトコンストラクタ
	Sample():m_pImg(nullptr), m_iWidth(-1), m_iHeight(-1) {}
	//コピーコンストラクタ
	Sample(const Sample& rhs){

		int size = rhs.m_iWidth * rhs.m_iHeight;

		if(rhs.m_pImg == nullptr)
			m_pImg = nullptr;
		else
		{
		#ifdef USE_UNIQUE_PTR
			m_pImg = std::unique_ptr<T[]>(new T[size]);
			//std::copy(rhs.m_pImg, rhs.m_pImg + sizeof(T) * size , m_pImg);
			memcpy(m_pImg.get(), rhs.m_pImg.get(), sizeof(T)*size);
		#else
			m_pImg = new T[size];
			memcpy(m_pImg, rhs.m_pImg, sizeof(T)*size);
		#endif
		}
		m_iWidth = rhs.m_iWidth;
		m_iHeight = rhs.m_iHeight;

	}

	//デストラクタ
	~Sample(){
#ifndef USE_UNIQUE_PTR
		delete m_pImg;
#endif
	}

	//コピー代入演算子 EffectiveC++のp52方式でやってみる
	Sample& operator = (const Sample& rhs)
	{

		if(rhs.m_pImg == nullptr)
		{
	#ifdef USE_UNIQUE_PTR
			m_pImg = nullptr;
	#else
			delete m_pImg;
			m_pImg = nullptr;
	#endif
		}
		else
		{
			int size = rhs.m_iWidth * rhs.m_iHeight;
	#ifdef USE_UNIQUE_PTR
			std::unique_ptr<T[]> pOrig = std::move(m_pImg);
			m_pImg = std::unique_ptr<T[]>(new T[size]);
			memcpy(m_pImg.get(), rhs.m_pImg.get(), sizeof(T)*size);
	#else
			T* pOrig = m_pImg;
			m_pImg = new T[size];
			memcpy(m_pImg, rhs.m_pImg, sizeof(T)*size);
			delete pOrig;
	#endif
		}
		m_iWidth = rhs.m_iWidth;
		m_iHeight = rhs.m_iHeight;

		return *this;
	}

	//イメージ生成 //ここで
	int createImage(const int width, const int height)
	{
		if(width <= 0 || height <= 0) return -1;
		m_iWidth = width;
		m_iHeight = height;
		int size = m_iWidth * m_iHeight;

#ifdef USE_UNIQUE_PTR
		std::unique_ptr<T[]> pOrig = std::move(m_pImg);
		m_pImg = std::unique_ptr<T[]>(new T[size]);
		memset(m_pImg.get(), 0, sizeof(T)*size);
#else
		T* pOrig = m_pImg;	//ここもnew失敗の例外安全を考えたらこうなるのかな？
		m_pImg = new T[size];
		memset(m_pImg, 0, sizeof(T)*size);
		delete pOrig;	//newが成功したら一応とっておいたものは消す。
#endif
		return 0;
	}

	//イメージ取得
	const T* getImagePointer()const
	{
#ifdef USE_UNIQUE_PTR
		return m_pImg.get();
#else
		return m_pImg;
#endif
	}

	//画像幅ゲッタ
	int width()const {return m_iWidth;}
	//画像高さゲッタ
	int height()const {return m_iHeight;}

private:
#ifdef USE_UNIQUE_PTR
	std::unique_ptr<T[]> m_pImg;
#else
	T* m_pImg;
#endif
	int m_iWidth;
	int m_iHeight;

};

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	int width = 4;
	int height = 3;
	qDebug() << "create Sample class 1 instance with defalut constructor";
	Sample<float> s1;
	qDebug() << "create imageData in class 1 ";

	s1.createImage(width + 1, height + 1);
	qDebug() << "s1( w , h ) = (" << s1.width() << "," << s1.height() << ")";

	s1.createImage(width, height);
	qDebug() << "s1( w , h ) = (" << s1.width() << "," << s1.height() << ")";

	qDebug() << "create Sample class 2 instance with copy constructor(from s1)";
	Sample<float> s2 = s1;
	qDebug() << "s2( w , h ) = (" << s2.width() << "," << s2.height() << ")";

	const float* pcfImg = s2.getImagePointer();
	float* pfImg = const_cast<float*>(pcfImg);
	for(int i = 0; i < width * height; ++i)
	{
		*pfImg = 1.0f * i;
		pfImg++;
	}
	pcfImg = s2.getImagePointer();
	for(int i = 0; i < width * height; ++i)
	{
		std::cout << (*pcfImg) << ",";
		pcfImg++;
	}
	std::cout << std::endl;

	qDebug() << "create Sample class 3 instance with defalut constructor";
	Sample<float> s3;
	qDebug() << "s3( w , h ) = (" << s3.width() << "," << s3.height() << ")";

	qDebug() << "copy Sample class 1 to Sample class 3 with operator =";
	s3 = s2;
	qDebug() << "s3( w , h ) = (" << s3.width() << "," << s3.height() << ")";
	pcfImg = s3.getImagePointer();
	for(int i = 0; i < width * height; ++i)
	{
		std::cout << (*pcfImg) << ",";
		pcfImg++;
	}
	std::cout << std::endl;

	qDebug() << "copy Sample class 1 to Sample class 1 with operator =";
	s1 = s1;
	qDebug() << "s1( w , h ) = (" << s1.width() << "," << s1.height() << ")";
	pcfImg = s1.getImagePointer();
	qDebug() << "s1:image array address =" << pcfImg;
	if(nullptr != pcfImg)
	{
		for(int i = 0; i < width * height; ++i)
		{
			std::cout << (*pcfImg) << ",";
			pcfImg++;
		}
		std::cout << std::endl;
	}

	qDebug() << "create Sample class 5 instance with defalut constructor";
	Sample<float> s5;
	pcfImg = s5.getImagePointer();
	qDebug() << "s1:image array address =" << pcfImg;
	if(nullptr != pcfImg)//nullptrチェックが欠かせないのはしょうがないのかな・・？
	{
		for(int i = 0; i < width * height; ++i)
		{
			std::cout << (*pcfImg) << ",";
			pcfImg++;
		}
		std::cout << std::endl;
	}

	return 0;//a.exec();
}
