#ifndef FPS_H
#define FPS_H

#include <QObject>
#include <QElapsedTimer>

class Fps : public QObject
{

public:
	explicit Fps(QObject *parent = nullptr);

signals:

public slots:
	void frameSwapped();

private:
	QElapsedTimer m_clElapsedTimer;

};	//<- missing ';'

#endif // FPS_H
