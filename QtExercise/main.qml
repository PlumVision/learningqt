import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 1.0

ApplicationWindow {
    visible: true
    width: 600
    height: 600
    title: qsTr("Qt Excercise")

    ListView {
        anchors.fill: parent
        model: 100
        delegate: Item {
            Rectangle {
                width: 600
                height: 100
                color: "black"

                Label {
                    id: textLabel
                    text: "Short Text"
                    color: "steelblue"
                    font.pointSize: 40
                    anchors.centerIn: parent
                }

                DropShadow {
                    anchors.fill: textLabel
                    radius: 12.0
                    samples: 25
                    color: "#90b4d2"
                    source: textLabel
                }
            }
        }
    }
}
