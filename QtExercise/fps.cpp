#include "fps.h"
#include <QDebug>

Fps::Fps(QObject *parent) : QObject(parent)
{
	m_clElapsedTimer.start();
}

void Fps::frameSwapped()
{
#if 1
	qint64 elapsed = m_clElapsedTimer.elapsed();
	float fps = 1000.0f / elapsed;
	qDebug() << fps << "fps";
	m_clElapsedTimer.start();
#else
	qDebug()<<Q_FUNC_INFO;
#endif
}
