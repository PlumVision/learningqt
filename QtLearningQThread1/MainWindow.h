#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//前方参照
class TaskSample1;

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

signals:
	void sigTask1Start();
	void sigTask2Start();

private slots:
	void on_btnStart_clicked();
	void on_action_Quit_triggered();
	void slotCountFinished1();
	void slotCountFinished2();
	void slotCount1(int);
	void slotCount2(int);

	void on_btnStop_clicked();

private:
	Ui::MainWindow *ui;

	void init();

	TaskSample1* m_pclTask1;
	TaskSample1* m_pclTask2;
	QThread* m_pclThread1;
	QThread* m_pclThread2;

	bool m_bTask1Finished;
	bool m_bTask2Finished;

};

#endif // MAINWINDOW_H
