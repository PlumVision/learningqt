#ifndef TASKSAMPLE1_H
#define TASKSAMPLE1_H

#include <QObject>

class TaskSample1 : public QObject
{
	Q_OBJECT
public:
	explicit TaskSample1(QObject *parent = nullptr);

signals:
	void sigCount(int);
	void sigCountFinished();

public slots:
	void count();
	void stop();

private:
	//このクラスはコピーを禁ず
	TaskSample1(const TaskSample1&);
	TaskSample1& operator = (const TaskSample1&);

	bool m_bAbort;

};

#endif // TASKSAMPLE1_H
