#include "TaskSample1.h"
#include <QThread>
#include <QDebug>
#include <windows.h>

TaskSample1::TaskSample1(QObject *parent) : QObject(parent),
	m_bAbort(false)
{

}

void TaskSample1::count()
{
	qDebug() << "count start @" << QThread::currentThread();

	for(int i = 0; i < 1000; i++)
	{
		if(m_bAbort)
		{
			m_bAbort = false;
			break;
		}
		Sleep(10);
		emit sigCount(i);
	}

	qDebug() << "count finished @" << QThread::currentThread();
	emit sigCountFinished();
}

void TaskSample1::stop()
{
	qDebug() << "count stop @" <<  QThread::currentThread();

	m_bAbort = true;
}
