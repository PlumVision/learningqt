#include "MainWindow.h"
#include <QApplication>

//-----------------------------------------------------------
//デバッグ用　メモリリーク検出の仕掛け
#ifdef _DEBUG
	#ifdef Q_OS_WIN
		#define _USE_VLD
		#ifdef _USE_VLD
//			#include <vld.h>
		#endif

		//#define _USE_CRTDBG
		#ifdef _USE_CRTDBG
			#define malloc(X) _malloc_dbg(X,_NORMAL_BLOCK,__FILE__,__LINE__)
			#define new ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
		#endif
	#endif	//#ifdef Q_OS_WIN
#endif	//#ifdef _DEBUG
//-----------------------------------------------------------

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow w;
	w.show();

	return a.exec();
}
