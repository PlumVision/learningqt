#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "TaskSample1.h"
#include <QThread>
#include <QDebug>
#include <QElapsedTimer>

#define _TEST1

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	m_bTask1Finished(false), m_bTask2Finished(false)
{
	ui->setupUi(this);

	init();
}

void MainWindow::init()
{
	m_pclTask1 = new TaskSample1;
	m_pclThread1 = new QThread;

	m_pclTask2 = new TaskSample1;
	m_pclThread2 = new QThread;

#ifdef _TEST1
	m_pclTask1->moveToThread(m_pclThread1);
	m_pclThread1->start();

	m_pclTask2->moveToThread(m_pclThread2);
	m_pclThread2->start();
#endif

	QObject::connect(this, SIGNAL(sigTask1Start()),
					 m_pclTask1, SLOT(count()));
	QObject::connect(this, SIGNAL(sigTask2Start()),
					 m_pclTask2, SLOT(count()));
	QObject::connect(m_pclTask1, SIGNAL(sigCount(int)),
					 this, SLOT(slotCount1(int)));
	QObject::connect(m_pclTask2, SIGNAL(sigCount(int)),
					 this, SLOT(slotCount2(int)));
	QObject::connect(m_pclTask1, SIGNAL(sigCountFinished()),
					 this, SLOT(slotCountFinished1()));
	QObject::connect(m_pclTask2, SIGNAL(sigCountFinished()),
					 this, SLOT(slotCountFinished2()));




}

MainWindow::~MainWindow()
{
	if(m_pclThread1->isRunning())
	{
		m_pclTask1->stop();
		m_pclThread1->exit();
		m_pclThread1->wait();
	}
	delete m_pclTask1;
	delete m_pclThread1;

	if(m_pclThread2->isRunning())
	{
		m_pclTask2->stop();
		m_pclThread2->exit();
		m_pclThread2->wait();
	}
	delete m_pclTask2;
	delete m_pclThread2;

	delete ui;
}

void MainWindow::on_btnStart_clicked()
{
	qDebug() << "btn start clicked @ " << QThread::currentThread();

	ui->btnStart->setEnabled(false);
	ui->btnStop->setEnabled(true);

	QElapsedTimer et;
	et.start();
#ifndef _TEST1
	m_pclTask1->moveToThread(m_pclThread1);
	m_pclThread1->start();
#endif
	emit sigTask1Start();
	qint64 elapsed = et.elapsed();
	qDebug() << "start thread1 time" << elapsed << "msec";

	et.start();
#ifndef _TEST1
	m_pclTask2->moveToThread(m_pclThread2);
	m_pclThread2->start();
#endif
	emit sigTask2Start();
	elapsed = et.elapsed();
	qDebug() << "start thread2 time" << elapsed << "msec";
}

void MainWindow::on_action_Quit_triggered()
{
	this->close();
}

void MainWindow::slotCountFinished1()
{
	m_bTask1Finished = true;
	if(m_bTask2Finished)
	{
		ui->btnStart->setEnabled(true);
		ui->btnStop->setEnabled(false);
		m_bTask1Finished = false;
		m_bTask2Finished = false;
	}
}

void MainWindow::slotCountFinished2()
{
	m_bTask2Finished = true;
	if(m_bTask1Finished)
	{
		ui->btnStart->setEnabled(true);
		ui->btnStop->setEnabled(false);
		m_bTask1Finished = false;
		m_bTask2Finished = false;
	}
}

void MainWindow::slotCount1(int value)
{
	ui->spinBox1->setValue(value);
}

void MainWindow::slotCount2(int value)
{
	ui->spinBox2->setValue(value);
}

void MainWindow::on_btnStop_clicked()
{
	m_pclTask1->stop();
	m_pclTask2->stop();

	ui->btnStart->setEnabled(true);
	ui->btnStop->setEnabled(false);
}
