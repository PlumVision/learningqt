#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

//前方参照
class GraphicsViewWithPointer;
class QGraphicsScene;
class QGraphicsProxyWidget;
class Form1;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void slotViewPress(QPoint);
	void slotViewMove(QPoint);
	void slotViewRelease(QPoint);

	void slotFormPress(QPoint);
	void slotFormMove(QPoint);
	void slotFormRelease(QPoint);

	void on_action_Quit_triggered();

private:
	Ui::MainWindow *ui;

	GraphicsViewWithPointer* m_pclView;
	QGraphicsScene* m_pclScene;
	Form1* m_pclForm1;
	QGraphicsProxyWidget* m_pclForm1Proxy;


	void createObjects();
};

#endif // MAINWINDOW_H
