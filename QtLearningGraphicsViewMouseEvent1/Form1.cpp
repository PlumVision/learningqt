#include "Form1.h"
#include "ui_Form1.h"
#include <QMouseEvent>

Form1::Form1(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::Form1)
{
	ui->setupUi(this);
}

Form1::~Form1()
{
	delete ui;
}

void Form1::mousePressEvent(QMouseEvent *event)
{
	emit sigPress(event->pos());

	//QWidget::mousePressEvent(event);

}

void Form1::mouseMoveEvent(QMouseEvent *event)
{
	emit sigMove(event->pos());

	//QWidget::mouseMoveEvent(event);
}

void Form1::mouseReleaseEvent(QMouseEvent *event)
{
	emit sigRelease(event->pos());

	//QWidget::mouseReleaseEvent(event);
}

void Form1::on_pushButton_clicked()
{
	if(ui->label->text() != "Hello Qt!")
		ui->label->setText("Hello Qt!");
	else
		ui->label->setText("Press the button");
}
