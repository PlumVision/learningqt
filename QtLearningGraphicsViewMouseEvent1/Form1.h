#ifndef FORM1_H
#define FORM1_H

#include <QWidget>

namespace Ui {
class Form1;
}

class Form1 : public QWidget
{
	Q_OBJECT

public:
	explicit Form1(QWidget *parent = 0);
	~Form1();

signals:
	void sigPress(QPoint);
	void sigMove(QPoint);
	void sigRelease(QPoint);

protected:
	void mousePressEvent(QMouseEvent* event);   //!<マウスボタン押下イベント処理
	void mouseMoveEvent(QMouseEvent* event);    //!<マウスドラッグ移動中イベント処理
	void mouseReleaseEvent(QMouseEvent* event); //!<マウスドラッグ確定イベント処理

private slots:
	void on_pushButton_clicked();

private:
	Ui::Form1 *ui;
};

#endif // FORM1_H
