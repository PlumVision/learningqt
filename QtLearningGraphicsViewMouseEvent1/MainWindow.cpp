#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "GraphicsViewWithPointer.h"
#include "Form1.h"
#include <QGraphicsScene>
#include <QDebug>
#include <QGraphicsProxyWidget>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	m_pclView(nullptr), m_pclScene(nullptr), m_pclForm1(nullptr),
	m_pclForm1Proxy(nullptr)
{
	ui->setupUi(this);
	createObjects();
}

MainWindow::~MainWindow()
{


	if(nullptr != m_pclForm1)
	{
		delete m_pclForm1;
		m_pclForm1 = nullptr;
	}

//	if(nullptr != m_pclForm1Proxy)
//	{
//		delete m_pclForm1Proxy;
//		m_pclForm1Proxy = nullptr;
//	}

	if(nullptr != m_pclScene)
	{
		delete m_pclScene;
		m_pclScene = nullptr;
	}

	if(nullptr != m_pclView)
	{
		delete m_pclView;
		m_pclView = nullptr;
	}

	delete ui;
}

void MainWindow::createObjects()
{

	m_pclView = new GraphicsViewWithPointer();
	ui->gridLayout->addWidget(m_pclView);

	m_pclView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_pclView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	m_pclScene = new QGraphicsScene(-10, 10, 480, 720);
	m_pclView->setScene(m_pclScene);

	m_pclForm1 = new Form1();
	m_pclForm1->setGeometry(0, 0, 480, 720);

	m_pclForm1Proxy = new QGraphicsProxyWidget;
	m_pclForm1Proxy->setWidget(m_pclForm1);


	m_pclScene->addItem(m_pclForm1Proxy);

	QObject::connect(m_pclView, SIGNAL(sigPress(QPoint)),
					 this, SLOT(slotViewPress(QPoint)));
	QObject::connect(m_pclView, SIGNAL(sigMove(QPoint)),
					 this, SLOT(slotViewMove(QPoint)));
	QObject::connect(m_pclView, SIGNAL(sigRelease(QPoint)),
					 this, SLOT(slotViewRelease(QPoint)));

	QObject::connect(m_pclForm1, SIGNAL(sigPress(QPoint)),
					 this, SLOT(slotFormPress(QPoint)));
	QObject::connect(m_pclForm1, SIGNAL(sigMove(QPoint)),
					 this, SLOT(slotFormMove(QPoint)));
	QObject::connect(m_pclForm1, SIGNAL(sigRelease(QPoint)),
					 this, SLOT(slotFormRelease(QPoint)));


}

void MainWindow::slotViewPress(QPoint pos)
{
	qDebug() << "ViewPress( x , y )=(" << pos.x() << "," << pos.y() << ")";
}
void MainWindow::slotViewMove(QPoint pos)
{
	qDebug() << "ViewMove( x , y )=(" << pos.x() << "," << pos.y() << ")";
}
void MainWindow::slotViewRelease(QPoint pos)
{
	qDebug() << "ViewRelease( x , y )=(" << pos.x() << "," << pos.y() << ")";
}

void MainWindow::slotFormPress(QPoint pos)
{
	qDebug() << "FormPress( x , y )=(" << pos.x() << "," << pos.y() << ")";
}

void MainWindow::slotFormMove(QPoint pos)
{
	qDebug() << "FormMove( x , y )=(" << pos.x() << "," << pos.y() << ")";
}

void MainWindow::slotFormRelease(QPoint pos)
{
	qDebug() << "FormRelease( x , y )=(" << pos.x() << "," << pos.y() << ")";
}


void MainWindow::on_action_Quit_triggered()
{
	qApp->quit();
}
