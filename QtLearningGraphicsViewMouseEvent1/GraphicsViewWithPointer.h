#ifndef GRAPHICSVIEWWITHPOINTER_H
#define GRAPHICSVIEWWITHPOINTER_H

#include <QGraphicsView>

class GraphicsViewWithPointer : public QGraphicsView
{
	Q_OBJECT
public:
	GraphicsViewWithPointer();

signals:
	void sigPress(QPoint);
	void sigMove(QPoint);
	void sigRelease(QPoint);

protected:
	void mousePressEvent(QMouseEvent* event);   //!<マウスボタン押下イベント処理
	void mouseMoveEvent(QMouseEvent* event);    //!<マウスドラッグ移動中イベント処理
	void mouseReleaseEvent(QMouseEvent* event); //!<マウスドラッグ確定イベント処理

};

#endif // GRAPHICSVIEWWITHPOINTER_H
