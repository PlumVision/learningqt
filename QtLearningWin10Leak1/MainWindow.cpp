#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QFileDialog>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_btnChooseFile_clicked()
{
	QString filepath = QFileDialog::getOpenFileName(this, "Choose a file to open",
													"./", "all file (*.*)");
	if(filepath.isEmpty())
		return;

	qDebug() << filepath;
}
