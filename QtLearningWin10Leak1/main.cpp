#include "MainWindow.h"
#include <QApplication>

#ifdef Q_OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow w;
	w.show();

	//char* p = new char[1];

	return a.exec();
}
