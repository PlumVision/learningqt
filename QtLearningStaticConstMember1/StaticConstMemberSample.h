#ifndef STATICCONSTMEMBERSAMPLE_H
#define STATICCONSTMEMBERSAMPLE_H

#include <string>

class StaticConstMemberSample
{
public:
	StaticConstMemberSample();

private:
	static const int s_iNumber = 5;
	static const bool s_bBool = true;
	static const char s_cChar = 'c';

	//error
	//static const float s_fNumber = 5.0f;
	//static const std::string s_strString("Hello world!");
	//Ok
	static const float s_fNumber;
	static const std::string s_strString;

	//OK@MSVC
	int m_iArray[s_iNumber];

	//enum hack
	enum {NumElements = 5};
	//Also OK@MSVC
	int m_iArray2[NumElements];

};

#endif // STATICCONSTMEMBERSAMPLE_H
