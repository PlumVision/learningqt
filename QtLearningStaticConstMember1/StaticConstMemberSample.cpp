#include "StaticConstMemberSample.h"
#include <iostream>

//OK
const float StaticConstMemberSample::s_fNumber = 3.14f;
const std::string StaticConstMemberSample::s_strString("Hello world!");

StaticConstMemberSample::StaticConstMemberSample()
	//NG :s_fNumber(5.0f)
	//NG : s_strString("Hello world!")
{
	//NG s_fNumber = 5.0f;
	//NG s_strString = std::string("Hello world!");

	std::cout << "s_iNumber: " << s_iNumber << ", s_bBool: " << s_bBool
			  << ", s_cChar: " << s_cChar << ", s_fNumber: " << s_fNumber << std::endl;
	std::cout << "s_strString: " << s_strString << std::endl;

	for(int i = 0; i < NumElements; i++)
	{
		std::cout << "m_iArray[" << i << "] = " << m_iArray[i]
					<< "\tm_iArray2[" << i << "] = " << m_iArray2[i] << std::endl;
	}
}
