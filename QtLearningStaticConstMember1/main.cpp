#include "Widget.h"
#include <QApplication>
#include "StaticConstMemberSample.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	Widget w;
	w.show();

	StaticConstMemberSample s;

	return a.exec();
}
