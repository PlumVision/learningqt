#include <QCoreApplication>
#include <QDebug>
#include <vector>

#ifdef Q_OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif

//class hoge{

//public:
//	const auto getValue()const{ return m_value; }
//private:
//	auto m_value = 1.0;//error C2853: 'm_value': 非静的データ メンバーは、'auto' を含む型にできません

//};

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);

	auto a = 1;
	auto b = 0.5;

	auto c = a + b;

	qDebug() << a << "+" << b << "=" << c;

//	hoge Hoge;
//	qDebug() << "Hoge::value =" << Hoge.getValue();

	//std::vector<auto> vec;//error C3539: テンプレート引数として、'auto' を含む型を指定することはできません
	std::vector<int> vec;
	vec.push_back(1.5);//intにdoubleを突っ込んでもintにキャストされちゃう
	vec.push_back(2.5);
	vec.push_back(3.5);

	qDebug() << "for(auto el: vec)";
	for(auto el: vec)
	{
		qDebug() << el;
	}

	qDebug() << "for(auto it = begin(vec);...";
	for(auto it = begin(vec); it != end(vec); ++it)
	{
		qDebug() << *it;
	}


	return 0;//a.exec();
}
