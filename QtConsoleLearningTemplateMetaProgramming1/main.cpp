#include <QCoreApplication>
#include <QDebug>

template<int X, int Y>
struct Add
{
	enum {result = X + Y};
};

template<unsigned n>
struct Factrorial
{
	enum { value = n * Factrorial<n - 1>::value};
};

template<>
struct Factrorial<0>
{
	enum { value = 1};
};

auto main(int argc, char *argv[]) -> int
{
	QCoreApplication a(argc, argv);

	qDebug() << "Add<2, 3> = " << Add<2, 3>::result;
	qDebug() << "Factrorial<6> =" << Factrorial<6>::value;

	return a.exec();
}
