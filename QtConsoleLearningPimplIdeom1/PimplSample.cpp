#include "PimplSample.h"

class PimplSample::Private
{
public:
	Private():value(0){}
	int value;
};

PimplSample::PimplSample():
	d(new Private)
{

}

PimplSample::~PimplSample()
{
	delete d;
}

const int PimplSample::getValue() const
{
	return d->value;
}

void PimplSample::setValue(const int value)
{
	if(d->value == value) return;
	d->value = value;
}

