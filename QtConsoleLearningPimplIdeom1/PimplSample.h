#ifndef PIMPLSAMPLE_H
#define PIMPLSAMPLE_H


class PimplSample
{
public:
	PimplSample();
	~PimplSample();

	const int getValue() const;
	void setValue(const int);

private:

	//ここは、ExceptionalC++本ではなく、鈴木佑さんd-pointer方式に沿ってみる
	//https://qiita.com/task_jp/items/3e992f61ede57c69168b
	class Private;
	Private* d;
};

#endif // PIMPLSAMPLE_H
