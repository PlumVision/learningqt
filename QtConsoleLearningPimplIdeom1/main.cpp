#include <QCoreApplication>
#include <QDebug>
#include "PimplSample.h"


int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	PimplSample s;

	auto value = 5;
	qDebug() << "set value " << value;
	s.setValue(value);

	auto getValue = s.getValue();
	qDebug() << "get value = " << getValue;

	return 0;//a.exec();
}
