#include <QCoreApplication>
#include <QDebug>

#ifdef Q_OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif

//クラステンプレートを学ぶ
template <typename T>
class myClass{
public:
	const T Value()const{return value;}
	void setValue(T v){value = v;}

private:
	T value;
};

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);

	myClass<int> m;
	m.setValue(1234.5);
	qDebug() << m.Value();





	//------------------------------
	int a=10;
	   int b=20;
	   int c=30;
	   a=b=c;
	   qDebug() << "a=" << a << " b=" << b << " c=" << c << "\n";

	   a=10;
	   b=20;
	   c=30;
	   a=(b=c);
	   qDebug() << "a=" << a << " b=" << b << " c=" << c << "\n";

	   a=10;
	   b=20;
	   c=30;
	   (a=b)=c;
	   qDebug() << "a=" << a << " b=" << b << " c=" << c << "\n";

	   return 0;

	return 0;//a.exec();
}
