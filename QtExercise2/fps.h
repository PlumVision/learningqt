#ifndef FPS_H
#define FPS_H

//forward declaration
class QElapsedTimer;

#include <QObject>
#include <QStringList>

class Fps : public QObject
{
	Q_OBJECT

public:
	explicit Fps(QObject *parent = nullptr);	//!<constructor
	~Fps();										//!<destructor

signals:

public slots:
    void frameSwapped();

private:
	QElapsedTimer* m_pclElapsedTimer;		//!<pointer to elapsed timer
};

class DataProvider : public QObject
{
    Q_OBJECT
public:
    explicit DataProvider(QObject* parent = nullptr);

public slots:
    int count() const;
    QString text(int index);

private:
    QStringList list;
};

#endif // FPS_H
