#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickWindow>
#include <QObject>

#include "fps.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    Fps fps;

    QQmlApplicationEngine engine;
    DataProvider dataProvider;
    engine.rootContext()->setContextProperty("Provider", &dataProvider);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QObject::connect((QQuickWindow*)QGuiApplication::topLevelWindows().at(0), &QQuickWindow::frameSwapped, &fps, &Fps::frameSwapped);

    return app.exec();
}
