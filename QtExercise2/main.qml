import QtQuick 2.0
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.0

Window {
    visible: true
    width: 600
    height: 600
    title: qsTr("Qt Excercise")
    color: "black"


    //Exercise 3
    ListView {
       id: perfProblem
        anchors.fill: parent
        model: 100
        delegate: Component {
            Rectangle {
                width: 600
                height: 100
                color: "transparent"

                Label {
                    id: textLabel
                    text: "Short Text"
                    color: "steelblue"
                    font.pointSize: 40
                    anchors.centerIn: parent
                }

                DropShadow {
                    anchors.fill: textLabel
                    radius: 12.0
                    samples: 25
                    color: "#90b4d2"
                    source: textLabel
                }
            }
        }
    }


   //EXERCISE 4: Uncomment to work on exercise 4
    ListView {
        id: myfonts
        anchors.fill: parent
        model: Provider.count()//3 //I know I defined 3 different font names in c++... how to show the names here...
        delegate: Component {
            Rectangle {
                width: 600
                height: 100
                color: "transparent"

                Label {
                    id: textLabel
                    text: Provider.text(index)//"TEXT" //TODO: I Want to show the font name here from Dataprovider....
                    color: "steelblue"
                    font.pointSize: 40
                    anchors.centerIn: parent
                }
            }
        }
        Component.onCompleted: perfProblem.visible = false;
    }
}
