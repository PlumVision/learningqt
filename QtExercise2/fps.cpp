#include "fps.h"
#include <QDebug>
#include <QElapsedTimer>

//Exercise 2
Fps::Fps(QObject *parent) : QObject(parent),
	m_pclElapsedTimer(new QElapsedTimer)
{
	m_pclElapsedTimer->start();
}

Fps::~Fps()
{
	delete m_pclElapsedTimer;
}

void Fps::frameSwapped()
{
	qint64 elapsed = m_pclElapsedTimer->elapsed();
	float fps = 1000.0f / elapsed;
	qDebug() << fps << "fps";
	m_pclElapsedTimer->start();
}

//Exercise 4
DataProvider::DataProvider(QObject* parent) : QObject (parent)
{
	list << "Arial" << "Helvetica" << "Times"
		 << "Wingdings" << "Verdana" << "Calibri"
		 << "Courier New" << "Segoe UI" << "Tahoma";
    //TODO: I want to add more fonts to the list, such as "Wingdings", "Verdana", "Calibri", "Courier New", "Segoe UI" and "Tahoma"
}

int DataProvider::count() const {
#if 0
	return 3;
#else
	return list.count();
#endif
}

QString DataProvider::text(int index) {
    //TODO: I wonder if it's possible to return the string from the 'list'??
#if 0
    if(index == 0) return "Arial";
    if(index == 1) return "Helvetica";
    if(index == 2) return "Times";

    return "NoMatchingIndex";
#else
	if(index < 0 || list.count() < index)
		return "NoMatchingIndex";
	return list.at(index);
#endif
}
