#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QObject>
#include "DataProvider.h"


int main(int argc, char *argv[])
{

	QGuiApplication app(argc, argv);

	QQmlApplicationEngine engine;

	DataProvider dataProvider;
	engine.rootContext()->setContextProperty("Provider", &dataProvider);

	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;


	return app.exec();
}
