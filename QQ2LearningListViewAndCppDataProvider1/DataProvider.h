#ifndef DATAPROVIDER_H
#define DATAPROVIDER_H
#include <QObject>

class DataProvider : public QObject
{
	Q_OBJECT

public:
	explicit DataProvider(QObject* parent = nullptr) : QObject(parent)
	{
		list << "Arial" << "Helvetica" << "Times" << "Wingdings"
			<< "Verdana" << "Calbri" << "Courier new" << "Segoe UI" << "Tahoma";
	}

public slots:
	int count() const
	{
		return list.count();
	}
	QString text(int index)
	{
		if(index < 0 || list.count() < index)
			return "NoMatchingIndex";

		return list.at(index);
	}

private:
	QStringList list;

};

#endif // DATAPROVIDER_H
