import QtQuick 2.8
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.0

Window {
    visible: true
    width: 640
    height: 900
    title: qsTr("Hello World")


    ListView{
        id: myfonts
        anchors.fill: parent
        model: Provider.count()

        delegate: Component{
            Rectangle{
                width: 640
                height: 100
                color: "transparent"

                Label{
                    id: textLabel
                    text: Provider.text(index)
                    color: "steelblue"
                    font.pointSize: 40
                    anchors.centerIn: parent
                }
            }
        }
    }

}
