//Exceptional C++ の第1問をアレンジ

#include <QCoreApplication>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <algorithm>

#ifdef Q_OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

//	本のとおり打ち込み始めたけど、Dateのクラス定義が面倒そうなのでやめた。
//	std::vector<int> e;
//	copy(std::istream_iterator<QDate>(cin),
//		 std::istream_iterator<QDate>(),
//		 std::back_inserter(e));
//	std::vector<int>::iterator first =
//			find(e.begin(), e.end(), 0);
//	std::vector<

//　vectorはint型にして、あらかじめ値を与えておくことにした。
	//-----------------prepareing a vector--------------------------
#if 1
	std::vector<int> vec = {2, 4, 6, 8};
#else
	std::vector<int> vec;	//空のvectorでもうまく動くかな？
#endif
	std::copy(vec.begin(), vec.end(),
		 std::ostream_iterator<int>(std::cout, ","));
	std::cout << std::endl;

	//-----------------searching 1--------------------------
	int searchNo1 = 3;
	std::cout << "finding element = " << searchNo1 << std::endl;

	std::vector<int>::iterator first
			= find(vec.begin(), vec.end(), searchNo1);
	if(first != vec.end())
	{
		std::size_t index = std::distance(vec.begin(), first);
		std::cout << "index of element = " << searchNo1 << " is " << index  << std::endl;
	}
	else
	{
		std::cout << "element = " << searchNo1 <<" was not found" << std::endl;
	}

	//-----------------searching 2 & change value--------------------------
	int searchNo2 = 9;
	int replaceNo = 10;
	std::cout << "finding element = " << searchNo2 << std::endl;

	std::vector<int>::iterator last
			= find(vec.begin(), vec.end(), searchNo2);
	if(last != vec.end())
	{
		std::size_t index = std::distance(vec.begin(), last);
		std::cout << "found element = " << searchNo2 << " element index = " << index  << std::endl;
		std::cout << "change element = " << searchNo2 << " to " << replaceNo  << std::endl;
		(*last) = replaceNo;
	}
	else
	{
		std::cout << "element = " << searchNo2 <<" was not found" << std::endl;
	}

#if 0
	std::copy(first, last,
		 std::ostream_iterator<int>(std::cout, ","));	//教科書の誤り例
#else
	std::copy(vec.begin(), vec.end(),
		 std::ostream_iterator<int>(std::cout, ","));
	std::cout << std::endl;
#endif

	//--------------------inserting--------------------------------

	int insertNo = 11;
	std::cout << "inserting " << insertNo << " into just before the last element" << std::endl;

	if(vec.size() <= 0)	//空のvectorにはinsertするとエラー落ちするので回避
	{
		std::cout << "inserting value =" << insertNo << ", but vector is emplty... abort" << std::endl;
	}
	else
	{
#if 0
		vec.insert(--vec.end(), insertNo);	//教科書の誤り例
#else
		vec.insert(vec.end() - 1, insertNo);	//空のVectorに対しては無効な処理になってしまう。
#endif
	}

#if 0
	copy(first, last,
		 std::ostream_iterator<int>(std::cout, "\n"));	//教科書の誤り例
#else
	copy(vec.begin(), vec.end(),
		 std::ostream_iterator<int>(std::cout, ","));
	std::cout << std::endl;
#endif

	return 0;//a.exec();
}
