﻿#include <QCoreApplication>
#include <QDebug>
//#define EXCEPTION_UNSAFE
#include "Stack.h"

#ifdef Q_OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	qDebug() << u8"デフォルトコンストラクタでs1<int>生成";
	Stack<int> s1;
	qDebug() << u8"s1に1,2,3をプッシュ";
	for(int i = 0; i < 3; ++i)
		s1.Push(i);
	qDebug() << "s1.Count() =" << s1.Count();

	qDebug() << u8"コピーコンストラクタでs2<int>を生成(s1がコピー元)";
	Stack<int> s2 = s1;
	qDebug() << "s2.Count() =" << s2.Count();

	qDebug() << u8"デフォルトコンストラクタでs3<int>生成";
	Stack<int> s3;
	qDebug() << "s3.Count() =" << s3.Count();

	qDebug() << u8"コピー代入演算子でs3にs1を代入";
	s3 = s1;
	qDebug() << "s3.Count() =" << s3.Count();

#ifdef EXCEPTION_UNSAFE //例外安全でないPopを使った問題を浮き彫りにする例
	try{
		for(int i = 0; i < 4; ++i)
			qDebug() << "s3.pop() = " << s3.Pop();
	}
	catch(const char* s)
	{
		qDebug() << s;
	}


	qDebug() << u8"デフォルトコンストラクタでs4<std::string>を生成";
	Stack<std::string> s4;
	qDebug() << u8"s4にstring’aaa’をプッシュ";
	s4.Push(std::string("aaa"));
	qDebug() << "s4.Count() =" << s4.Count();

	qDebug() << u8"文字列1にs4からpopしたのを格納(std::stringコピーコンストラクタ)";
	std::string str1 = s4.Pop();
	qDebug() << u8"文字列1=" << str1.c_str();
	qDebug() << "s4.Count() =" << s4.Count();

	std::string str2;
	qDebug() << u8"文字列2にs4からpopしたのを格納(std::stringコピー代入演算子)";
	str2 = s4.Pop();	//!<例外発生！クラッシュ！！ そっか、先のPopでもう要素がなくなってしまった！
	qDebug() << u8"文字列2=" << str2.c_str();
	qDebug() << "s4.Count() =" << s4.Count();
#else
	try{

		qDebug() << u8"s3のtopで先頭要素を参照し、popで先頭を取り出す";
		for(int i = 0; i < 4; ++i)
		{
			qDebug() << "s3.Top() = " << s3.Top();
			qDebug() << "s3.Pop()";
			s3.Pop();
		}
	}
	catch(const char* s)
	{
		qDebug() << s;
	}

	qDebug() << u8"デフォルトコンストラクタでs4<std::string>を生成";
	Stack<std::string> s4;
	qDebug() << u8"s4にstring’aaa’をプッシュ";
	s4.Push(std::string("aaa"));
	qDebug() << "s4.Count() =" << s4.Count();

	qDebug() << u8"文字列1にs4からTop()したのを格納(std::stringコピーコンストラクタ)";
	std::string str1 = s4.Top();
	qDebug() << u8"文字列1=" << str1.c_str();
	qDebug() << "s4.Count() =" << s4.Count();

	std::string str2;
	qDebug() << u8"文字列2にs4からTopしたのを格納(std::stringコピー代入演算子)";
	str2 = s4.Top();	//!<Topなら要素は減らない。
	qDebug() << u8"文字列2=" << str2.c_str();
	qDebug() << "s4.Count() =" << s4.Count();
#endif

	return 0;//a.exec();
}
