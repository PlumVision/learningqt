﻿#ifndef STACK_H
#define STACK_H

#include <assert.h>

template<class T>
class Stack
{
public:
	Stack();	//!<デフォルトコンストラクタ
	Stack(const Stack&);	//!<コピーコンストラクタ
	~Stack() noexcept;	//!<デストラクタは例外を投げない！ということを宣言してしまうのがnoexceptなようだ。

	Stack& operator =(const Stack&);	//!<コピーコンストラクタ

	size_t Count()const;	//!<要素数のゲッタ
	void Push(const T&);	//!<Push受け口
#ifdef EXCEPTION_UNSAFE
	T Pop();				//!<Pop受け口　もし空なら例外を投げる
#else
	void Pop();			//!Pop受け口
	T& Top();				//!Top受け口
#endif


private:
	T* newCopy(const T*, size_t, size_t);	//!<コピーヘルパー関数
	T* m_pArray;	//!<確保したメモリ領域へのポインタ
	size_t m_ullSize;	//!<確保した要素数
	size_t m_ullUsed;	//!<実際に使用中の要素数
};

//デフォルトコンストラクタ実装
template<class T>
Stack<T>::Stack():
	m_pArray(new T[10]),		//デフォルトの確保、このnewが失敗したらStackオブジェクトは生成されないのでリークは起こらない。
	m_ullSize(10), m_ullUsed(0)
{
}

//コピーコンストラクタ実装
template<class T>
Stack<T>::Stack(const Stack<T> &rhs):
	m_pArray(newCopy(rhs.m_pArray,
					 rhs.m_ullSize,
					 rhs.m_ullSize)),
	m_ullSize(rhs.m_ullSize),
	m_ullUsed(rhs.m_ullUsed)
{

}

//デストラクタ実装
template<class T>
Stack<T>::~Stack()
{
	delete[] m_pArray;	//deleteは例外を投げてはいけないことになっているらしいので、ここでの失敗は考えなくていいらしい。
}

//コピー代入演算子の実装
template<class T>
Stack<T>& Stack<T>::operator =(const Stack<T>& rhs)
{
		if(this != &rhs)
		{
			T* arrayNew = newCopy(rhs.m_pArray, rhs.m_ullSize, rhs.m_ullSize);
			delete[] m_pArray;			//deleteは例外を投げられない。
			m_pArray = arrayNew;		//所有権を受け取る
			m_ullSize = rhs.m_ullSize;
			m_ullUsed = rhs.m_ullUsed;
		}
		return *this;	//安全。コピーを呼び出さない。
}

//コピーヘルパー関数実装
template<class T>
T* Stack<T>::newCopy(const T* src,
					 size_t srcsize,
					 size_t dstsize)
{
	assert(dstsize >= srcsize);
	T* dst = new T[dstsize];		//ここで例外が起きてもdst配列自体は生成されないからリークにならないってことか。
	try
	{
		std::copy(src, src + srcsize, dst);	//コピー時に例外が起きちゃうと確保したdst配列がリークしちゃうからtryで囲む
	}
	catch(...)
	{
		delete[] dst;	//これはもともと例外を投げることができない
		throw;			//上位へそのままなげて例外中立を保つ
	}
	return dst;
}

//要素数のゲッタの実装
template<class T>
size_t Stack<T>::Count() const
{
	return m_ullUsed;
}

//Push受け口の実装
template<class T>
void Stack<T>::Push(const T& t)
{
	if(m_ullUsed == m_ullSize)	//必要であれば、ある拡大係数で拡大する
	{
		size_t arraySizeNew = m_ullSize * 2 + 1;
		T* arrayNew = newCopy(m_pArray, m_ullSize, arraySizeNew);
		delete[] m_pArray;		//deleteは例外を投げれない
		m_pArray = arrayNew;	//所有権を取得
		m_ullSize = arraySizeNew;
	}
	m_pArray[m_ullUsed] = t;
	++m_ullUsed;
}

//Pop受け口の実装
#ifdef EXCEPTION_UNSAFE //例外安全でないバージョン
template<class T>
T Stack<T>::Pop()
{
	if(m_ullUsed == 0)
	{
		throw "pop from empty stack";
	}
	else
	{
		T result = m_pArray[m_ullUsed - 1];
		--m_ullUsed;
		return result;
	}
}
#else
template<class T>
void Stack<T>::Pop()
{
	if(m_ullUsed == 0)
	{
		throw "empty stack";
	}
	else
	{
		--m_ullUsed;
	}
}

template<class T>
T& Stack<T>::Top()
{
	if(m_ullUsed == 0)
	{
		throw "pop from empty stack";
	}
	else
	{
		return m_pArray[m_ullUsed - 1];
	}
}
#endif

#endif // STACK_H
