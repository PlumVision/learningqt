#include "Plot.h"
#include <qwt_plot_curve.h>
#include <qwt_legend.h>
#include <qwt_plot_grid.h>


void MyQwtPlotCurve::drawCurve( QPainter *painter, int style, const QwtScaleMap &xMap, const QwtScaleMap &yMap, const QRectF &canvasRect, int from, int to ) const{

    int preceding_from = from;
    bool is_gap = true;

    // Scan all data to identify gaps
    for (int i = from; i <= to; i++){
        const QPointF sample = /*d_series*/this->sample(i);

        // In a gap
        if(sample.y() >= 0 && is_gap){ // wait for the curve to be positive again
            preceding_from = i;
            is_gap = false;
        }

        // At the beginning of a gap (or the end of the serie) : draw the preceding interval
        if((sample.y() < 0 && !is_gap) || (i == to && sample.y() >= 0)){
            drawSteps(painter, xMap, yMap, canvasRect, preceding_from, i>from ? i-1 : i); // or drawLines, drawSticks, drawDots
            is_gap = true;
        }
    }
}

Plot::Plot(QWidget *parent) :QwtPlot(parent)
{

	setCanvasBackground(QColor(Qt::white)); // 背景白
	insertLegend(new QwtLegend(), QwtPlot::BottomLegend);   // 凡例

	// グリッド
	   QwtPlotGrid *grid1 = new QwtPlotGrid;
	   grid1->enableXMin(true);
	   grid1->setMajorPen(QPen(Qt::black, 0, Qt::DashDotLine));
	   grid1->setMinorPen(QPen(Qt::gray, 0, Qt::DotLine));
	   grid1->attach(this);

	   // 曲線の設定
       //sin_crv = new QwtPlotCurve("sin");
       sin_crv = new MyQwtPlotCurve();
	   sin_crv->setPen(QPen(Qt::red));
	   sin_crv->attach(this);


	   // 曲線の描画
	   plotCurve();
}

void Plot::plotCurve()
{
	const int kArraySize = 1000;

	double x[kArraySize] = {}; // x
	double y_sin[kArraySize] = {}; // y

	for (int i = 0; i < kArraySize; ++i) {
		x[i] = i / (kArraySize - 1.0);
        y_sin[i] = sin(2.0*2.0*M_PI*(x[i]));
	}

    sin_crv->setSamples(x, y_sin, kArraySize);
}
