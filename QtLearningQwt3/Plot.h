#ifndef PLOT_H
#define PLOT_H

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
//class QwtPlotCurve;

class MyQwtPlotCurve : public QwtPlotCurve
{
protected:
    virtual void drawCurve( QPainter *p, int style, const QwtScaleMap &xMap, const QwtScaleMap &yMap, const QRectF &canvasRect, int from, int to ) const;
};


class Plot : public QwtPlot
{
	Q_OBJECT

public:
	explicit Plot(QWidget* parent = 0);
	void plotCurve();

private:
    //QwtPlotCurve* sin_crv;
    MyQwtPlotCurve* sin_crv;
};

#endif // PLOT_H
