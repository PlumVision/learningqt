import QtQuick 2.0
import QtQuick.Window 2.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Starting Point")

    Timer {
		interval: 1
        repeat: true
        running: true
        onTriggered: {
			rot.angle += 1;
            if(rot.angle === 360) {
                console.log("full rotation");
                rot.angle = 0;
            }
        }
    }

    Rectangle {
        id: gauge
        width: 200
        height: 200
        color: "blue"
        anchors.centerIn: parent
    }

    Rectangle {
        id: needle
        width: 10
        height: 90
        color: "red"
        x: gauge.x + (gauge.width/2) - (needle.width / 2)
        y: gauge.y + (gauge.height/2)
        transform: Rotation {
            id: rot
            angle: 45
        }
    }
}
