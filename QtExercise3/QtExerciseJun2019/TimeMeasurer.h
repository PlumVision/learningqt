#ifndef TIMEMEASURER_H
#define TIMEMEASURER_H

class QElapsedTimer;

#include <QObject>

class TimeMeasurer : public QObject
{
	Q_OBJECT
public:
	explicit TimeMeasurer(QObject *parent = nullptr);	//!<constructor
	~TimeMeasurer();									//!<destructor

public slots:
	void slotStart();	//!<slot for measuring start
	void slotEnd();		//!<slot for measuring end

private:
	QElapsedTimer* m_pclElapsedTimer;	//!<pointer to elapsedTimer
};

#endif // TIMEMEASURER_H
