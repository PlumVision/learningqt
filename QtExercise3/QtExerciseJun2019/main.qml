import QtQuick 2.9
import QtQuick.Window 2.2

Window {
	visible: true
	width: 1920
	height: 720
	title: qsTr("Qt Exercise Jun 2019")
	property bool bMeasuring: false

	signal sigStart();
	signal sigEnd();

	//BackGround
	Image{
		id: background
		anchors.fill: parent
		source: "qrc:/assets/background"
	}

	//Gauge
	Image{
		id: gauge
		width: 620
		height: 620
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.verticalCenter:  parent.verticalCenter
		source: "qrc:/assets/gauge"
		focus: true

		Keys.onPressed: {
			if(!Keys.isAutoRepeat){
				switch(event.key){
				case Qt.Key_Up:
					if(!bMeasuring && needle.rotation === 45){
						sigStart();
						bMeasuring = true;
					}
					animToMax.start()
					break;
				case Qt.Key_Down:
					animToMin.start()
					break;
				default:
				}
			}
		}

		Keys.onReleased: {
			if(!Keys.isAutoRepeat){
				switch(event.key){
				case Qt.Key_Up:
					animToMax.stop()
					break;
				case Qt.Key_Down:
					animToMin.stop()
					break;
				default:
				}
			}
		}

	}//Image(Gauge)

	Image {
		id: needle
		width: 90
		height: 300
		x: gauge.x  + (gauge.width / 2) - (needle.width / 2)
		y: gauge.y  + (gauge.height / 2)
		source: "qrc:/assets/needle"
		rotation: 45
		transformOrigin: Item.Top
		property int iSpeed: 0

		onRotationChanged: {
			//calculate speed from rpm
			iSpeed = (rotation - 45) * 200 / 270
			speed.text = iSpeed.toString() + "\nkm/h";

			//change needle color aroud 6500 rpm
			if(rotation < 263)
				needle.source = "qrc:/assets/needle"
			else
				needle.source = "qrc:/assets/needleRed"

			//end measuring time at 315deg (start at 45 deg)
			if(rotation === 315 && bMeasuring){
				sigEnd()
				bMeasuring = false
			}
		}

		PropertyAnimation{
			id: animToMax
			target: needle
			property: "rotation"
			duration: (315 - needle.rotation) / 270 * 8000
			to: 315
		}

		PropertyAnimation{
			id: animToMin
			target: needle
			property: "rotation"
			duration: (needle.rotation - 45) / 270 * 8000
			to: 45
		}

	}//Image(needle)

	Text {
		id: speed
		horizontalAlignment: Text.AlignHCenter
		text: "0\nkm/h"
		font.pointSize:60
		font.family: "DejaVu Sans Mono"
		font.bold: true
		color: "white"
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.verticalCenter: parent.verticalCenter
	}//Text(speed)

}//Window
