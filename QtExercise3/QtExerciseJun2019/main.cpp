#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "TimeMeasurer.h"

int main(int argc, char *argv[])
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QGuiApplication app(argc, argv);

	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	TimeMeasurer meas;
	QObject* root = engine.rootObjects().first();

	QObject::connect(root, SIGNAL(sigStart()),
					 &meas, SLOT(slotStart()));
	QObject::connect(root, SIGNAL(sigEnd()),
					 &meas, SLOT(slotEnd()));

	return app.exec();
}
