#include "TimeMeasurer.h"
#include <QElapsedTimer>
#include <QDebug>

//constructor
TimeMeasurer::TimeMeasurer(QObject *parent) : QObject(parent),
	m_pclElapsedTimer(new QElapsedTimer())
{

}

//destructor
TimeMeasurer::~TimeMeasurer()
{
	delete m_pclElapsedTimer;
}

//slot for measuring start
void TimeMeasurer::slotStart()
{
	m_pclElapsedTimer->start();
	qDebug() << "TimeMeasurer: start";
}

//slot for measuring end
void TimeMeasurer::slotEnd()
{
	qint64 elapsed = m_pclElapsedTimer->elapsed();
	qDebug() << "TimeMeasurer: elapsed" << elapsed << "msec";
}
