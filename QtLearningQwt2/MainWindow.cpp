#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "Plot.h"
#include <QHBoxLayout>


MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),m_pclPlot(NULL),m_pclLayout(NULL)
{
	ui->setupUi(this);

	m_pclPlot = new Plot();

	m_pclLayout = new QHBoxLayout(ui->centralWidget);
	m_pclLayout->setContentsMargins( 0, 0, 0, 0 );
	m_pclLayout->addWidget( m_pclPlot );

	ui->pushButton->setText(tr("button"));

}

MainWindow::~MainWindow()
{
	delete ui;
}
