#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class Plot;
class QHBoxLayout;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private:
	Ui::MainWindow *ui;

	Plot* m_pclPlot;
	QHBoxLayout* m_pclLayout;
};

#endif // MAINWINDOW_H
