#-------------------------------------------------
#
# Project created by QtCreator 2017-09-13T18:59:32
#
#-------------------------------------------------

QT       += core gui #svg printsupport opengl concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtLearningQwt2
TEMPLATE = app

DEFINES += UNICODE WIN32 QT_DLL QWT_DLL

INCLUDEPATH += $(QWTROOT)/include

SOURCES += main.cpp\
        MainWindow.cpp \
    Plot.cpp

HEADERS  += MainWindow.h \
    Plot.h

FORMS    += MainWindow.ui

CONFIG += mobility
MOBILITY = 
win32:{
LIBS += -L$(QWTROOT)/minGW/lib \
#LIBS += -L$(QWTROOT)/x64/lib \
		-lqwt
}

linux:{
	android:{
		equals(ANDROID_TARGET_ARCH, armeabi-v7a){
			LIBS += -L$(QWTROOT)/armv7/lib \
					-lqwt
		}
		equals(ANDROID_TARGET_ARCH, x86)  {
			LIBS += -L$(QWTROOT)/x86Android/lib \
					-lqwt
		}
	}
}

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
	ANDROID_EXTRA_LIBS = \
		C:\Qwt-6.1.4-svn\armv7\lib\libqwt.so
}


contains(ANDROID_TARGET_ARCH,x86) {
	ANDROID_EXTRA_LIBS = \
		C:\Qwt-6.1.4-svn\x86Android\lib\libqwt.so
}

RESOURCES += \
    QtLearningQwt2.qrc

