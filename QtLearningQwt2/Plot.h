#ifndef PLOT_H
#define PLOT_H
#include <qwt_scale_draw.h>
#include <qwt_plot.h>
#include <QDateTime>

class QwtPlotCurve;
class QwtPlotPanner;

class TimeScaleDraw: public QwtScaleDraw {
public:
TimeScaleDraw(const QDateTime &base) : baseDateTime(base) {
  setLabelRotation(0);
  setLabelAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  setSpacing(20);
}
virtual QwtText label(double v) const {
  QDateTime dateTime = baseDateTime.addMSecs(v);
  return dateTime.toString("yyyy/MM/dd");
}

private:
QDateTime baseDateTime;
};

class Plot : public QwtPlot
{
	Q_OBJECT

public:
	explicit Plot(QWidget* parent = 0);
	void plotCurve();
protected:
	virtual void resizeEvent( QResizeEvent * );
private:
	QwtPlotCurve* sin_crv;
	QwtPlotPanner* m_pPanner;
};

#endif // PLOT_H
