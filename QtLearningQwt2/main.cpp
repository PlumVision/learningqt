#include "MainWindow.h"
#include <QApplication>
#include <QHBoxLayout>
#include <QTranslator>



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QTranslator translator;
		translator.load(":/trans_ja");
			//trans_ja.qmというファイルのファイル名のみを渡す。
		a.installTranslator(&translator);
			//QTranslatorクラスを適用

	MainWindow w;
	//QHBoxLayout *layout = new QHBoxLayout( );


	w.show();

	return a.exec();
}
