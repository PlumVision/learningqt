#include "Plot.h"
#include <qwt_plot_curve.h>
#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_panner.h>
#include <QDateTime>
#include <QList>
#include <QDataStream>
#include <QDebug>


Plot::Plot(QWidget *parent) :QwtPlot(parent)
{

	setCanvasBackground(QColor(Qt::white)); // 背景白
	insertLegend(new QwtLegend(), QwtPlot::BottomLegend);   // 凡例

	// グリッド
	   QwtPlotGrid *grid1 = new QwtPlotGrid;
	   grid1->enableXMin(true);
	   grid1->setMajorPen(QPen(Qt::black, 0, Qt::DashDotLine));

	   grid1->setMinorPen(QPen(Qt::gray, 0, Qt::DotLine));
	   grid1->attach(this);

	   // 曲線の設定
	   sin_crv = new QwtPlotCurve(tr("sin"));
	   sin_crv->setPen(QPen(Qt::red));
	   sin_crv->attach(this);

	   m_pPanner = new QwtPlotPanner( canvas() );
m_pPanner->setOrientations(Qt::Horizontal);
	   // 曲線の描画
	   plotCurve();
}

void Plot::resizeEvent( QResizeEvent *event )
{
	QwtPlot::resizeEvent( event );

	// Qt 4.7.1: QGradient::StretchToDeviceMode is buggy on X11
	plotCurve();
}

void Plot::plotCurve()
{
#if 0
	const int kArraySize = 1000;

	double x[kArraySize] = {}; // x
	double y_sin[kArraySize] = {}; // y

	for (int i = 0; i < kArraySize; ++i) {
		x[i] = i / (kArraySize - 1.0);
		y_sin[i] = sin(2.0*M_PI*(x[i]));
	}

	sin_crv->setSamples(x, y_sin, kArraySize);
#else
	const int numData = 12;
	QDateTime current = QDateTime::currentDateTime();
	QList<QDateTime> xlist;
	for(int i = 0; i < numData; i++)
	{
		QDateTime dt = current.addMonths(i);
		qDebug() << dt;
		xlist.push_back(dt);
	}

	double x[numData];
	double y[numData];
	for(int i = 0; i < numData; i++)
	{
		x[i] = xlist.at(i).toMSecsSinceEpoch();
		y[i] = i * i;
	}

	QDateTime epoch = QDateTime::fromMSecsSinceEpoch(0);
	setAxisScaleDraw(QwtPlot::xBottom, new TimeScaleDraw(epoch));
	sin_crv->setSamples(x, y, numData);


#endif
}
