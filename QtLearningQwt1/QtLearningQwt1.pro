#-------------------------------------------------
#
# Project created by QtCreator 2017-09-02T17:51:02
#
#-------------------------------------------------

QT       += core gui svg printsupport opengl concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtLearningQwt1
TEMPLATE = app

DEFINES += QWT_DLL

INCLUDEPATH += $(QWTROOT)/include

SOURCES += main.cpp\
        MainWindow.cpp \
    Plot.cpp

HEADERS  += MainWindow.h \
    Plot.h

FORMS    += MainWindow.ui

CONFIG += mobility
MOBILITY = 

win32:{
#LIBS += -L$(QWTROOT)/minGW/lib \
LIBS += -L"C:\Qwt-6.1.4-svn\x64\lib" \
		-lqwt
}

linux:{
	android:{
		equals(ANDROID_TARGET_ARCH, armeabi-v7a){
			LIBS += -L$(QWTROOT)/armv7/lib \
					-lqwt
		}
		equals(ANDROID_TARGET_ARCH, x86)  {
			LIBS += -L$(QWTROOT)/x86Android/lib \
					-lqwt
		}
	}

	 linux-g++:{
		CONFIG(debug, debug|release){
			LIBS += -L$(QWTROOT)/lib/debug \
					-lqwt
		}
		else{
			LIBS += -L$(QWTROOT)/lib \
					-lqwt
		}
	}

}

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
	ANDROID_EXTRA_LIBS = \
		C:\Qwt-6.1.4-svn\armv7\lib\libqwt.so
}


contains(ANDROID_TARGET_ARCH,x86) {
	ANDROID_EXTRA_LIBS = \
		C:\Qwt-6.1.4-svn\x86Android\lib\libqwt.so
}


