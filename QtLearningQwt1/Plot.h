#ifndef PLOT_H
#define PLOT_H

#include <qwt_plot.h>

class QwtPlotCurve;

class Plot : public QwtPlot
{
	Q_OBJECT

public:
	explicit Plot(QWidget* parent = 0);
	void plotCurve();

private:
	QwtPlotCurve* sin_crv;
};

#endif // PLOT_H
