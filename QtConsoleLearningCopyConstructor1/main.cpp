﻿#include <QCoreApplication>
#include <QDebug>

#ifdef Q_OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif

class Sample
{
public:
	//デフォルトコンストラクタ
	Sample(){
		qDebug() << u8"デフォルトコンストラクタが呼ばれました";
	}

	//初期値を与えるコンストラクタ
	Sample(const int value){
		this->values[0] = value;
		this->values[1] = value;
		this->values[2] = value;
		qDebug() << u8"初期値指定のコンストラクタが呼ばれました";
	}

#if 0//もしコピーコンストラクタをていぎしないと。。？ あれ？うまくいっているようだな。ああ、そうかPODなクラスだからうまくいくのか。
	//コピーコンストラクタ
	Sample(const Sample& rhs)
	{
		this->m_strObjectName = rhs.m_strObjectName;

		const int* pValuesArray = rhs.values;

		this->values[0] = pValuesArray[0];
		this->values[1] = pValuesArray[1];
		this->values[2] = pValuesArray[2];

		qDebug() << u8"コピーコンストラクタが呼ばれました";
	}

	//コピー代入演算子
	Sample& operator = (const Sample& rhs)
	{
		this->m_strObjectName = rhs.m_strObjectName;

		const int* pValuesArray = rhs.values;

		this->values[0] = pValuesArray[0];
		this->values[1] = pValuesArray[1];
		this->values[2] = pValuesArray[2];

		qDebug() << u8"コピー代入演算子が呼ばれました";
		return *this;
	}
#endif
//private:
	int values[3];	//サンプルなのでpublicにしちゃえ
	QString m_strObjectName;
};

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	//デフォルトコンストラクタ生成
	qDebug() << u8"s1生成(初期値指定なし)";
	Sample s1;
	s1.m_strObjectName = "s1";

	qDebug() << u8"s1の配列に値を設定";
	s1.values[0] = 2;
	s1.values[1] = 4;
	s1.values[2] = 6;

	//初期値を与えるコンストラクタで生成
	qDebug() << u8"s2生成(初期値5指定)";
	Sample s2(5);
	s2.m_strObjectName = "s2";

	//s1からコピーコンストラクタで生成
	qDebug() << u8"s3生成(=s1指定)";
	Sample s3 = s1;
	qDebug() << "s3.ObjectName :" << s3.m_strObjectName;
	s3.m_strObjectName = "s3";
	qDebug() << "s3.ObjectName(revise) :" << s3.m_strObjectName;

	//s2からコピーコントラクタで生成
	qDebug() << u8"s4生成(s2からコピーコンストラクト)";
	Sample s4 = s2;
	qDebug() << "s4.ObjectName :" << s4.m_strObjectName;
	s4.m_strObjectName = "s4";
	qDebug() << "s4.ObjectName(revise) :" << s4.m_strObjectName;

	qDebug() << u8"s2の配列に値をサンプルなので再設定";
	s2.values[0] = 3;
	s2.values[1] = 6;
	s2.values[2] = 9;

	//結果表示
	qDebug() << "index\ts1\ts2\ts3\ts4";
	for(size_t i = 0; i < 3; ++i)
	{
		qDebug() << i << "\t" << s1.values[i] << "\t" << s2.values[i] << "\t"
					 << s3.values[i] << "\t" << s4.values[i];
	}

	//s1にコピー代入演算子でs2を代入する
	qDebug() << u8"s1 = s2 (代入)";
	s1 = s2;
	qDebug() << "s1.ObjectName :" << s1.m_strObjectName;
	s1.m_strObjectName = "s1";
	qDebug() << "s1.ObjectName(revise) :" << s1.m_strObjectName;

	//結果表示
	qDebug() << "index\ts1\ts2\ts3\ts4";
	for(size_t i = 0; i < 3; ++i)
	{
		qDebug() << i << "\t" << s1.values[i] << "\t" << s2.values[i] << "\t"
					 << s3.values[i] << "\t" << s4.values[i];
	}

	//自己代入
	 qDebug() << u8"s1 = s1 (自己代入)";
	 //結果表示
	 qDebug() << "index\ts1\ts2\ts3\ts4";
	 for(size_t i = 0; i < 3; ++i)
	 {
		 qDebug() << i << "\t" << s1.values[i] << "\t" << s2.values[i] << "\t"
					  << s3.values[i] << "\t" << s4.values[i];
	 }

	return 0;//a.exec();
}
