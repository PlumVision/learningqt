#include <QCoreApplication>
#include <QDebug>
#include <vector>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	//後置インクリメントは常に前置インクリメントで実装される。
	//const T T::operator++(int)
	//{
	//		T old(*this);	//元の値を覚えておく（Tのコンストラクタが走る）
	//		++this;			//前置インクリメントを使って実装（先に前置インクリメントが定義されている必要がある）。
	//		return old;		//なんとインクリメントされる前の値が返されるが、実体はインクリメント済み！
	//}

	for(int i = 0; i < 5 ; i++)	//後置インクリメント　i++の戻り値を受け手があると、インクリメントする前の値が入ることになるが、この場合は受け手がない。ループ内でiを使うときには実体はインクリメント済みの値を得ることになる。ループのたびに新しいint型の変数が作られる。
		//最新のコンパイラではRVO(return value optimization)が働くので、組み込みの値型なら後置でもパフォーマンス低下はほとんどないようだ。
	{
		qDebug() << i;
	}
	qDebug() << "------------------------------";

	//前置インクリメント
	for(int j = 0; j < 5; ++j)	//前置インクリメント　++iの戻り値の受け手があると、インクリメント後の値が入ることになるが、この場合は受け手がない。ループ内でiを使うときは当然インクリメント済みの値を得ることになる。
	{
		qDebug() << j;
	}
	qDebug() << "------------------------------";

	int x = 0;
	qDebug() << x;

	//後置インクリメント結果の代入をためす。
	int y = x++;
	qDebug() << y << "," << x;	// yは0(インクリメント前のx) xは1

	qDebug() << "------------------------------";
	//前置インクリメント結果の代入を試す・
	int z = ++x;
	qDebug() << z << "," << x;	// zは2(xが1の状態でインクリメント後のx)　xも2

	qDebug() << "------------------------------";
	std::vector<int> vec;
	for(int i = 0; i < 5; ++i)	//ここは、効率的であるとされる++iを試そう。int型だからどっちでもそんなに変わらんはず。
		vec.push_back(i);

	std::vector<int>::iterator itTemp = vec.begin();
	for(std::vector<int>::iterator it0 = vec.begin(); it0 != vec.end(); itTemp = it0++)//後置インクリメントだと余分なイタレータの一時オブジェクトがイチイチ生成する
		qDebug() << (*itTemp) << ","<< (*it0);

	qDebug() << "------------------------------";
	itTemp = vec.begin();
	for(std::vector<int>::iterator it1 = vec.begin(); it1 != vec.end(); itTemp = ++it1)//前置インクリメントなら、イタレータの一時オブジェクトはできない
		qDebug() << (*itTemp) << "," << (*it1);

	qDebug() << "------------------------------";
	for(auto it = vec.begin(); it != vec.end(); ++it)	//C++11のautoを使ったらちょっと楽に書ける、でもインクリメントを前置するのはプログラマの責任
		qDebug() << (*it);

	qDebug() << "------------------------------";
	for(const auto& i : vec)	//こう書いたらもっと楽ちんやね。C++の範囲for万歳！？
		qDebug() << i;

	return 0;// a.exec();
}
