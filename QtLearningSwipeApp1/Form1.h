#ifndef FORM1_H
#define FORM1_H

#include <QWidget>

namespace Ui {
class Form1;
}

class Form1 : public QWidget
{
	Q_OBJECT

public:
	explicit Form1(QWidget *parent = 0);
	~Form1();

protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);

signals:
	//void sigMouseMoveH(int width);	//水平方向のドラッグ(スワイプ)量を発信
	//void sigMouseRelease();
	void sigMousePress(QPoint);
	void sigMouseMove(QPoint);
	void sigMouseRelease(QPoint);

private slots:
	void on_pushButton_clicked();

private:
	Ui::Form1 *ui;

	bool m_bIsDragging;	//!<ドラッグ中のフラグ
	QPoint m_clDragStartPoint;	//!<ドラッグ開始点

	void calcMouseMoveH(QPoint pos);
};

#endif // FORM1_H
