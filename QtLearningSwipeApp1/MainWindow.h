#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QEasingCurve>

//前方参照
class GraphicsViewWithPointer;	//class QGraphicsViewのマウスイベントを拾えるように継承し拡張
class QGraphicsScene;
class Form1;
class Form2;
class Form3;
class QGraphicsProxyWidget;
class QState;
class QStateMachine;
class QParallelAnimationGroup;
class QPropertyAnimation;
//class QEasingCurve;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);	//!<コンストラクタ
	~MainWindow();								//!<デストラクタ

signals:
	//各フォームへの移行開始を発信するシグナル
	void sigState1();
	void sigState2();
	void sigState3();

protected:
	bool eventFilter(QObject *watched, QEvent *event);//!<イベントフィルタ処理をオーバライドしてMainWindowに配置したオブジェクトのイベントを捕まえて先に処理する

private slots:
	//MainWindow内のWidgets操作時のシグナルを受けるスロット群
	void on_pushButton_clicked();
	void on_pushButton_2_clicked();
	void on_pushButton_3_clicked();
	void on_pushButton_4_clicked();
	void on_spinBoxAngle_valueChanged(int value);

	//GraphicsViewWithPointerからマウスイベント座標発信シグナルを受けるためのスロット群
	void slotMousePressView(QPoint);
	void slotMouseMoveView(QPoint);
	void slotMouseReleaseView(QPoint);

	//Form1～3からのマウスプレス・リリース座標発信シグナルを受けるためのスロット群
	void slotMousePressForms(QPoint);
	void slotMouseReleaseForms(QPoint);

	//Form1からのマウスイムーブ座標発信シグナルを受けるためのスロット群
	void slotMouseMoveForm1(QPoint);

	//Form2からのマウスムーブ座標発信シグナルを受けるためのスロット群
	void slotMouseMoveForm2(QPoint);

	//Form3からのマウスムーブ座標発信シグナルを受けるためのスロット群
	void slotMouseMoveForm3(QPoint);


private:
	void createObjects();	//!<オブジェクト生成
	void resizeObjects();	//!<画面サイズに合わせて大きさを調整
	void setAnimDuration(int msec);	//!<//アニメーションの実施時間を用意したアニメーション全部に同じ値を設定
	void setFormRotation(qreal degree);	//!<フォーム回転量を設定（すべてのフォームを一括で設定）
	int calcAnimDuration(qreal degree);//!<現在の回転量からアニメ時間を計算する（初期版）

	Ui::MainWindow *ui;	//UIを指すポインタ(QtCreatorによる自動生成）

	//ViewとScene
	GraphicsViewWithPointer* m_pclView;		//QGraphicsView* m_pclView;
	QGraphicsScene* m_pclScene;

	//Qtのフォームを伴うクラスを指すポインタ
	Form1* m_pclForm1;
	Form2* m_pclForm2;
	Form3* m_pclForm3;

	//各フォームに対応するプロキシウィジェットを指すポインタ
	QGraphicsProxyWidget* m_pclForm1Proxy;
	QGraphicsProxyWidget* m_pclForm2Proxy;
	QGraphicsProxyWidget* m_pclForm3Proxy;

	//状態管理を行うオブジェクト群へのポインタ
	QState* m_pclRootState;
	QState* m_pclState1;
	QState* m_pclState2;
	QState* m_pclState3;
	//QState* m_pclStateCur;	//どうやらいらないかも・・
	QStateMachine* m_pclStateMachine;

	//アニメーションを司るオブジェクト群へのポインタ
	QParallelAnimationGroup* m_pclAnimGroup;
	QPropertyAnimation* m_pclAnim1;
	QPropertyAnimation* m_pclAnim2;
	QPropertyAnimation* m_pclAnim3;

	//アニメ補間カーブオブジェクトへのポインタ
	//QEasingCurve* m_pclEasingCurve;
	QEasingCurve m_clEasingCurve;

	//その他メンバ変数
	int m_iViewWidth;		//GraphicsViewの幅を格納する
	int m_iViewHeight;		//GraphicsViewの高さを格納
	int m_iDeadZoneWidth;	//スワイプ時の不感帯の幅を格納
	bool m_bIsDragging;		//ドラッグ中かどうかを記録するフラグ
	QPoint m_clDragStartPos;	//ドラッグ開始時のView上の座標を格納
	QPoint m_clMouseMovePos;	//ドラッグ中のView上の座標を格納
	QPoint m_clDragEndPos;		//ドラッグ終了時のView上の座標を格納
	qreal m_dRotationCur;		//現在の回転量を格納
};

#endif // MAINWINDOW_H
