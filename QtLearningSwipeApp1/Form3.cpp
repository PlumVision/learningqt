#include "Form3.h"
#include "ui_Form3.h"
#include <QDebug>

Form3::Form3(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::Form3)
{
	ui->setupUi(this);
}

Form3::~Form3()
{
	delete ui;
}

void Form3::mousePressEvent(QMouseEvent *event)
{
	m_bIsDragging = true;
	m_clDragStartPoint = event->pos();
	emit sigMousePress(m_clDragStartPoint);

}
void Form3::mouseMoveEvent(QMouseEvent *event)
{
	if(m_bIsDragging)
	{
		emit sigMouseMove(event->pos());
	}
}

void Form3::mouseReleaseEvent(QMouseEvent *event)
{
	if(m_bIsDragging)
	{
		m_bIsDragging = false;
		emit sigMouseRelease(event->pos());
	}
}

//void Form3::calcMouseMoveH(QPoint pos)
//{
//	int moveH = pos.x() - m_clDragStartPoint.x();
//	qDebug() << "MouseMoveH = " << moveH;
//	emit sigMouseMoveH(moveH);
//}
