#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "GraphicsViewWithPointer.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <QDebug>
#include <QtWidgets>
#include <QParallelAnimationGroup>
#include <QPropertyAnimation>
#include <QStateMachine>
#include <QSignalTransition>
#include <QEasingCurve>
#include "Form1.h"
#include "Form2.h"
#include "Form3.h"

#define ANIM_DURATION (1200)

static const double dDefaultAmplitude = 1.0;

//コンストラクタ
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow), m_pclView(nullptr), m_pclScene(nullptr),
	m_pclForm1(nullptr), m_pclForm2(nullptr), m_pclForm3(nullptr),
	m_pclForm1Proxy(nullptr), m_pclForm2Proxy(nullptr), m_pclForm3Proxy(nullptr),
	m_pclRootState(nullptr), m_pclState1(nullptr), m_pclState2(nullptr), m_pclState3(nullptr), //m_pclStateCur(nullptr),
	m_pclStateMachine(nullptr), m_pclAnimGroup(nullptr),
	m_pclAnim1(nullptr), m_pclAnim2(nullptr), m_pclAnim3(nullptr),
	m_clEasingCurve(QEasingCurve::OutElastic),//m_pclEasingCurve(nullptr),
	m_iViewWidth(480), m_iViewHeight(720), m_iDeadZoneWidth(24),
	m_bIsDragging(false), m_clDragStartPos(QPoint(0, 0)),
	m_clMouseMovePos(QPoint(0, 0)), m_clDragEndPos(QPoint(0, 0)),
	m_dRotationCur(0.0)
{
	ui->setupUi(this);
	createObjects();
}

//デストラクタ
MainWindow::~MainWindow()
{
	//delete順注意

	delete m_pclState1;
	delete m_pclState2;
	delete m_pclState3;
	delete m_pclRootState;
	delete m_pclStateMachine;

	delete m_pclAnim1;
	delete m_pclAnim2;
	delete m_pclAnim3;

	delete m_pclAnimGroup;

	//delete m_pclEasingCurve;

	//プロキシをかフォームかどちらかを消すと引き連れて消されるようだ。
	//delete m_pclForm1;
	//delete m_pclForm2;
	//delete m_pclForm3;
	delete m_pclForm1Proxy;
	delete m_pclForm2Proxy;
	delete m_pclForm3Proxy;

	delete m_pclScene;
	delete m_pclView;

	delete ui;
}

//イベントフィルタ処理をオーバライドしてMainWindowに配置したオブジェクトのイベントを捕まえて先に処理する
bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
	if(watched == m_pclView)
	{
		if(event->type() == QEvent::Resize)
		{
			int w = m_pclView->width();
			int h = m_pclView->height();
			if(m_iViewWidth != w || m_iViewHeight != h)
			{
				m_iViewWidth = w;
				m_iViewHeight = h;
				m_iDeadZoneWidth = 0.05 * w;
				resizeObjects();
			}
		}
	}

	//デフォルト処理を読んでその戻り値を返して終了
	return QMainWindow::eventFilter(watched, event);
}

//オブジェクト生成
void MainWindow::createObjects()
{
	//GraphicsViewの生成と設定
	m_pclView = new GraphicsViewWithPointer;//m_pclView = new QGraphicsView;
	m_pclView->setCacheMode(QGraphicsView::CacheBackground);
	m_pclView->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
	m_pclView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_pclView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_pclView->installEventFilter(this);

	//あらかじめデザイナで用意しておいたgridLayoutにGraphicsViewをはめ込む
	ui->gridLayoutMain->addWidget(m_pclView);

	//ウインドウのサイズ（初期値）からフォームの回転中心からの配置位置を計算
	int w = m_iViewWidth;//m_pclView->width();//ui->graphicsView->width();
	int h = m_iViewHeight;//m_pclView->height();//ui->graphicsView->height();
	qDebug() << "graphicsViewMain ( w , h ) =" << w << "," << h << ")";
	qreal shift = static_cast<qreal>(w / 2) / tan(15 * M_PI / 180);
	qDebug() << "shift =" << shift;

	//GraphicsSceneを生成　(横方向の中心がx=0、縦方向の下端のｙ座標が上記で計算した配置位置になるように座標を設定）
	m_pclScene = new QGraphicsScene(-w / 2, -shift, w, h);

	//ViewにSceneを設定することにより、Sceneの内容がViewに表示される。
	m_pclView->setScene(m_pclScene);

	//Form1を生成し、Form1を回転させるため、対応するプロキシウィジェットを生成しForm1を設定。
	m_pclForm1 = new Form1;
	m_pclForm1->setGeometry(0, 0, w, h);
	m_pclForm1Proxy = new QGraphicsProxyWidget;
	m_pclForm1Proxy->setWidget(m_pclForm1);

	//プロキシウィジェットの方に回転中心と表示位置・初期回転量を設定
	m_pclForm1Proxy->setTransformOriginPoint(m_pclForm1->width() / 2,
											 m_pclForm1->height() + shift);
	m_pclForm1Proxy->setPos(- w / 2,  - shift);
	m_pclForm1Proxy->setRotation(0);

	//Form2の生成、設定、プロキシウィジェットの生成、設定
	m_pclForm2 = new Form2;
	m_pclForm2->setGeometry(0, 0, w, h);
	m_pclForm2Proxy = new QGraphicsProxyWidget;
	m_pclForm2Proxy->setWidget(m_pclForm2);
	m_pclForm2Proxy->setTransformOriginPoint(m_pclForm2->width() / 2,
											 m_pclForm2->height() + shift);
	m_pclForm2Proxy->setPos(-m_pclForm2->width() / 2,  - shift);
	m_pclForm2Proxy->setRotation(30);

	//Form3の生成、設定、プロキシウィジェットの生成、設定
	m_pclForm3 = new Form3;
	m_pclForm3->setGeometry(0, 0, w, h);
	m_pclForm3Proxy = new QGraphicsProxyWidget;
	m_pclForm3Proxy->setWidget(m_pclForm3);
	m_pclForm3Proxy->setTransformOriginPoint(m_pclForm3->width() / 2,
											 m_pclForm3->height() + shift);
	m_pclForm3Proxy->setPos(-m_pclForm3->width() / 2,  - shift);
	m_pclForm3Proxy->setRotation(60);

	//Sceneにプロキシウィジェットをアイテムとして追加
	m_pclScene->addItem(m_pclForm1Proxy);
	m_pclScene->addItem(m_pclForm2Proxy);
	m_pclScene->addItem(m_pclForm3Proxy);

	//状態管理マシーンで管理する状態を表すオブジェクトを生成
	m_pclRootState = new QState();	//親となるオブジェクトを生成
	m_pclState1 = new QState(m_pclRootState);	//状態1用のオブジェクトを生成
	m_pclState2 = new QState(m_pclRootState);	//状態2用のオブジェクトを生成
	m_pclState3 = new QState(m_pclRootState);	//状態3用のオブジェクトを生成
	//m_pclStateCur = new QState(m_pclRootState);	//現在の状態用のオブジェクトを生成（最終的にはいらないかも？）

	//状態1の時の各フォームの回転量を設定
	m_pclState1->assignProperty(m_pclForm1Proxy, "rotation", qreal(0));
	m_pclState1->assignProperty(m_pclForm2Proxy, "rotation", qreal(30));
	m_pclState1->assignProperty(m_pclForm3Proxy, "rotation", qreal(60));

	//状態2の時の各フォームの回転量を設定
	m_pclState2->assignProperty(m_pclForm1Proxy, "rotation", qreal(-30));
	m_pclState2->assignProperty(m_pclForm2Proxy, "rotation", qreal(0));
	m_pclState2->assignProperty(m_pclForm3Proxy, "rotation", qreal(30));

	//状態3の時の各フォームの回転量を設定
	m_pclState3->assignProperty(m_pclForm1Proxy, "rotation", qreal(-60));
	m_pclState3->assignProperty(m_pclForm2Proxy, "rotation", qreal(-30));
	m_pclState3->assignProperty(m_pclForm3Proxy, "rotation", qreal(0));

	//現在の状態用の各フォームの回転量を設定（初期は状態1なので、それと同じ量を代入しておく）
//	m_pclStateCur->assignProperty(m_pclForm1Proxy, "rotation", qreal(0));
//	m_pclStateCur->assignProperty(m_pclForm2Proxy, "rotation", qreal(30));
//	m_pclStateCur->assignProperty(m_pclForm3Proxy, "rotation", qreal(60));

	//状態管理マシーンを生成・設定
	m_pclStateMachine = new QStateMachine;
	m_pclStateMachine->addState(m_pclRootState);//親ステートを登録すると上記で定義した複数の状態を一度に登録できるようだ。
	m_pclStateMachine->setInitialState(m_pclRootState);//初期状態も親ステートに設定するのが作法？
	m_pclRootState->setInitialState(m_pclState1);//で親ステート内でも初期状態を設定するらしい。

	//アニメーション補間カーブの生成
	//m_pclEasingCurve = new QEasingCurve;
	//m_pclEasingCurve->setType(QEasingCurve::OutElastic);
	//m_pclEasingCurve->setAmplitude(dDefaultAmplitude);
	m_clEasingCurve.setAmplitude(dDefaultAmplitude);

	//アニメーショングループの生成
	m_pclAnimGroup = new QParallelAnimationGroup;

	//アニメーション1（Form1の回転アニメを司る）を生成・設定
	m_pclAnim1 = new QPropertyAnimation(m_pclForm1Proxy, "rotation");
	m_pclAnim1->setDuration(ANIM_DURATION);//アニメ時間を設定
	//m_pclAnim1->setEasingCurve(QEasingCurve::OutElastic);//OutBounce);//アニメ補間カーブを設定
	m_pclAnim1->setEasingCurve(m_clEasingCurve);
	m_pclAnimGroup->addAnimation(m_pclAnim1);//アニメーショングループに追加

	//アニメーション2(Form2の回転アニメを司る）を生成・設定→アニメグループに登録
	m_pclAnim2 = new QPropertyAnimation(m_pclForm2Proxy, "rotation");
	m_pclAnim2->setDuration(ANIM_DURATION);
	//m_pclAnim2->setEasingCurve(QEasingCurve::OutElastic);//OutBounce);
	m_pclAnim2->setEasingCurve(m_clEasingCurve);
	m_pclAnimGroup->addAnimation(m_pclAnim2);

	//アニメーション3(Form3の回転アニメを司る）を生成・設定→アニメグループに登録
	m_pclAnim3 = new QPropertyAnimation(m_pclForm3Proxy, "rotation");
	m_pclAnim3->setDuration(ANIM_DURATION);
	//m_pclAnim3->setEasingCurve(QEasingCurve::OutElastic);//OutBounce);
	m_pclAnim3->setEasingCurve(m_clEasingCurve);
	m_pclAnimGroup->addAnimation(m_pclAnim3);

	//QSignalTransactionはnewで生成せずに、親ステートのaddトランザクション追加メソッドで出来たオブジェクトをポインタで受けて
	//QSignalTransactionのaddAnimationメソッドでanimationGroupを追加すると対応付けができるらしい。
	//これを対応付けたいシグナル、状態、アニメグループの数分だけ行うようだ。
	QSignalTransition* trans;
	trans = m_pclRootState->addTransition(this,
								SIGNAL(sigState1()), m_pclState1);
	trans->addAnimation(m_pclAnimGroup);
	trans = m_pclRootState->addTransition(this,
								SIGNAL(sigState2()), m_pclState2);
	trans->addAnimation(m_pclAnimGroup);
	trans = m_pclRootState->addTransition(this,
								SIGNAL(sigState3()), m_pclState3);
	trans->addAnimation(m_pclAnimGroup);

	//GraphicsViewからのマウスイベント時の座標発信シグナルを受けて、このクラスのSLOTに流す設定
	QObject::connect(m_pclView, SIGNAL(sigPress(QPoint)),
						 this, SLOT(slotMousePressView(QPoint)));
	QObject::connect(m_pclView, SIGNAL(sigMove(QPoint)),
						 this, SLOT(slotMouseMoveView(QPoint)));
	QObject::connect(m_pclView, SIGNAL(sigRelease(QPoint)),
						 this, SLOT(slotMouseReleaseView(QPoint)));


	//Form1～3からのマウスプレス・リリース座標発信シグナルを受けて、このクラスのSLOTに流す設定
	QObject::connect(m_pclForm1, SIGNAL(sigMousePress(QPoint)),
						 this, SLOT(slotMousePressForms(QPoint)));
	QObject::connect(m_pclForm2, SIGNAL(sigMousePress(QPoint)),
						 this, SLOT(slotMousePressForms(QPoint)));
	QObject::connect(m_pclForm3, SIGNAL(sigMousePress(QPoint)),
						 this, SLOT(slotMousePressForms(QPoint)));

	QObject::connect(m_pclForm1, SIGNAL(sigMouseRelease(QPoint)),
						 this, SLOT(slotMouseReleaseForms(QPoint)));
	QObject::connect(m_pclForm2, SIGNAL(sigMouseRelease(QPoint)),
						 this, SLOT(slotMouseReleaseForms(QPoint)));
	QObject::connect(m_pclForm3, SIGNAL(sigMouseRelease(QPoint)),
						 this, SLOT(slotMouseReleaseForms(QPoint)));


	//Form1～3からのマウスムーブーム座標発信シグナルを受けての処理(signal-slot接続を前提）
	QObject::connect(m_pclForm1, SIGNAL(sigMouseMove(QPoint)),
						 this, SLOT(slotMouseMoveForm1(QPoint)));
	QObject::connect(m_pclForm2, SIGNAL(sigMouseMove(QPoint)),
						 this, SLOT(slotMouseMoveForm2(QPoint)));
	QObject::connect(m_pclForm3, SIGNAL(sigMouseMove(QPoint)),
						 this, SLOT(slotMouseMoveForm3(QPoint)));

	//状態マシーンをスタートさせる
	m_pclStateMachine->start();
	return;
}

//MainWindowのサイズ変更を受けてForm等のサイズを再設定
void MainWindow::resizeObjects()
{
	//MainWindowのリサイズイベント処理にてm_iViewWidth, m_iViewHeightに
	//リサイズ後のサイズが格納されているのでそれに合わせて各オブジェクトを設定
	int w = m_iViewWidth;
	int h = m_iViewHeight;
	qDebug() << "graphicsView ( w , h ) =" << w << "," << h << ")";

	//リサイズ後のGraphicsViewに対応したFormの表示位置を決めるshiftを再計算
	qreal shift = static_cast<qreal>(w / 2) / tan(15 * M_PI / 180);
	qDebug() << "shift =" << shift;

	//計算したshiftに合わせてForm1とそのプロキシを再設定
	m_pclForm1->setGeometry(0, 0, w, h);
	m_pclForm1Proxy->setTransformOriginPoint(w / 2, h + shift);
	m_pclForm1Proxy->setPos(- w / 2,  - shift);

	//計算したshiftに合わせてForm2とそのプロキシを再設定
	m_pclForm2->setGeometry(0, 0, w, h);
	m_pclForm2Proxy->setTransformOriginPoint(w / 2, h + shift);
	m_pclForm2Proxy->setPos(- w / 2,  - shift);

	//計算したshiftに合わせてForm3とそのプロキシを再設定
	m_pclForm3->setGeometry(0, 0, w, h);
	m_pclForm3Proxy->setTransformOriginPoint(w / 2, h + shift);
	m_pclForm3Proxy->setPos(- w / 2,  - shift);

	//Viewのサイズ変更に合わせてSceneの座標設定も再設定しておく必要がある。
	m_pclScene->setSceneRect(-w / 2, -shift, w, h);

	//すべてを設定し終えたらSceneを更新すると表示に反映される。
	m_pclScene->update();

}

//アニメーションの実施時間を用意したアニメーション全部に同じ値を設定
void MainWindow::setAnimDuration(int msec)
{
	m_pclAnim1->setDuration(msec);
	m_pclAnim2->setDuration(msec);
	m_pclAnim3->setDuration(msec);
}

//フォーム回転量を設定（すべてのフォームを一括で設定）
void MainWindow::setFormRotation(qreal degree)
{
	m_pclForm1Proxy->setRotation(degree);
	m_pclForm2Proxy->setRotation(degree + 30);
	m_pclForm3Proxy->setRotation(degree + 60);
	m_pclScene->update();
}

//現在の回転量からアニメ時間を計算する（初期版）
int MainWindow::calcAnimDuration(qreal degree)
{
	int duration_msec = ANIM_DURATION;

	if(degree <-60)
	{
		duration_msec = -(degree + 60) / 30 * ANIM_DURATION;
	}
	else if(-60 <= degree && degree <-45)
	{
		duration_msec = (degree + 60) / 30 * ANIM_DURATION;
	}
	else if(-45 <= degree && degree < -30)
	{
		duration_msec = -(degree + 30) / 30 * ANIM_DURATION;
	}
	else if(-30 <= degree && degree < -15)
	{
		duration_msec = (degree + 30) / 30 * ANIM_DURATION;
	}
	else if(-15 <= degree)// && degree <= 15)
	{
			duration_msec = -(degree ) / 30 * ANIM_DURATION;
	}
	qDebug() << "animation duration(msec) =" << duration_msec;

	return duration_msec;

}

//ボタン1押下時の処理
void MainWindow::on_pushButton_clicked()
{
	setAnimDuration(ANIM_DURATION);
	emit sigState1();
	ui->spinBoxAngle->setValue(0);
}

//ボタン2押下時の処理
void MainWindow::on_pushButton_2_clicked()
{
	setAnimDuration(ANIM_DURATION);
	emit sigState2();
	ui->spinBoxAngle->setValue(-30);
}

//ボタン3押下時の処理
void MainWindow::on_pushButton_3_clicked()
{
	setAnimDuration(ANIM_DURATION);
	emit sigState3();
	ui->spinBoxAngle->setValue(-60);
}

//スピンボックスの値が変更された時の処理
void MainWindow::on_spinBoxAngle_valueChanged(int degree)
{
	m_dRotationCur = static_cast<qreal>(degree);
	setFormRotation(m_dRotationCur);
}

//ボタン4押下時の処理
void MainWindow::on_pushButton_4_clicked()
{
	qreal degree = static_cast<qreal>(ui->spinBoxAngle->value());

	setAnimDuration(calcAnimDuration(degree));

	int degreeTarget = 0;
	if(-60 <= degree && degree < -45)
	{
		emit sigState3();
		degreeTarget = -60;
	}
	else if(-45 <= degree && degree < -15)
	{
		emit sigState2();
		degreeTarget = -30;
	}
	else if(-15 <= degree && degree < 0)
	{
		emit sigState1();
		degreeTarget = 0;
	}
	ui->spinBoxAngle->setValue(degreeTarget);
	m_dRotationCur = static_cast<qreal>(degreeTarget);

}

void MainWindow::slotMousePressView(QPoint pos)
{
	m_clDragStartPos = pos;
	qDebug() << "View pressed ( x , y ) = (" << m_clDragStartPos.x() << "," << m_clDragStartPos.y() << ")";
}

void MainWindow::slotMouseMoveView(QPoint pos)
{
	if(m_bIsDragging)
	{
		m_clMouseMovePos = pos;
		qDebug() << "View moved ( x , y ) = (" << m_clMouseMovePos.x() << "," << m_clMouseMovePos.y() << ")";
	}
}

void MainWindow::slotMouseReleaseView(QPoint pos)
{
	m_clDragEndPos = pos;
	qDebug() << "View released ( x , y ) = (" << m_clDragEndPos.x() << "," << m_clDragEndPos.y() << ")";
}


//フォーム1～3上でマウスボタン押下時の座標発信シグナルを受けての処理(signal-slot接続を前提）
void MainWindow::slotMousePressForms(QPoint pos)
{
	m_bIsDragging = true;
	qDebug() << "Clicked ( x , y ) = (" << pos.x() << "," << pos.y() << ")";
}

//フォーム1～3上でマウスドラッグ終時の座標発信シグナルを受けての処理(signal-slot接続を前提）
void MainWindow::slotMouseReleaseForms(QPoint pos)
{
	if(!m_bIsDragging)
		return;

	setAnimDuration(calcAnimDuration(m_dRotationCur));

	m_bIsDragging = false;
	qDebug() << "released ( x , y ) = (" << pos.x() << "," << pos.y() << ")";

	int degreeTarget = 0;
	if(-15 < m_dRotationCur)
	{
		emit sigState1();
		degreeTarget = 0;
	}
	else if(-45 < m_dRotationCur && m_dRotationCur <=-15)
	{
		emit sigState2();
		degreeTarget = -30;
	}
	else if(m_dRotationCur <= -45)//(-45 < m_dRotationCur || m_dRotationCur <=15)
	{
		emit sigState3();
		degreeTarget = -60;
	}
	ui->spinBoxAngle->setValue(degreeTarget);
	m_dRotationCur = static_cast<qreal>(degreeTarget);
}

//フォーム1上でマウスドラッグ進行中の座標発信シグナルを受けての処理(signal-slot接続を前提）
void MainWindow::slotMouseMoveForm1(QPoint pos)
{
	qDebug() << "moved ( x , y ) = (" << pos.x() << "," << pos.y() << ")";
	int moveH = m_clMouseMovePos.x() - m_clDragStartPos.x();
	qDebug() << "moveH =" << moveH;

	qreal degree = m_dRotationCur;
	if(abs(moveH) > m_iDeadZoneWidth)
		{
			if(0 <= moveH)
				degree = 30.0 * (moveH - m_iDeadZoneWidth) / m_iViewWidth;
			else
				degree= 30.0 * (moveH + m_iDeadZoneWidth) / m_iViewWidth;
		}

		if((-30.0 < degree && degree < 0.0 )||(0.0 < degree && degree < 15.0))
		{
			m_dRotationCur = degree;
			qDebug() << "currentRotation =" << m_dRotationCur;
			setFormRotation(m_dRotationCur);
		}
}

//フォーム2上でマウスドラッグ進行中の座標発信シグナルを受けての処理(signal-slot接続を前提）
void MainWindow::slotMouseMoveForm2(QPoint pos)
{
	qDebug() << "moved ( x , y ) = (" << pos.x() << "," << pos.y() << ")";
	int moveH = m_clMouseMovePos.x() - m_clDragStartPos.x();
	qDebug() << "moveH =" << moveH;

	qreal degree = m_dRotationCur;
	if(abs(moveH) > m_iDeadZoneWidth)
		{
			if(0 <= moveH)
				degree = -30.0 + 30.0 * (moveH - m_iDeadZoneWidth) / m_iViewWidth;
			else
				degree= -30.0 + 30.0 * (moveH + m_iDeadZoneWidth) / m_iViewWidth;
		}

		if((-60.0 < degree && degree < 0.0 ))
		{
			m_dRotationCur = degree;
			qDebug() << "currentRotation =" << m_dRotationCur;
			setFormRotation(m_dRotationCur);
		}
}


//フォーム3上でマウスドラッグ進行中の座標発信シグナルを受けての処理(signal-slot接続を前提）
void MainWindow::slotMouseMoveForm3(QPoint pos)
{
	qDebug() << "moved ( x , y ) = (" << pos.x() << "," << pos.y() << ")";
	int moveH = m_clMouseMovePos.x() - m_clDragStartPos.x();
	qDebug() << "moveH =" << moveH;

	qreal degree = m_dRotationCur;
	if(abs(moveH) > m_iDeadZoneWidth)
		{
			if(0 <= moveH)
				degree = -60.0 + 30.0* (moveH - m_iDeadZoneWidth) / m_iViewWidth;
			else
				degree= -60.0 + 30.0 * (moveH + m_iDeadZoneWidth) / m_iViewWidth;
		}

		if((-75.0 < degree && degree < -30.0 ))
		{
			m_dRotationCur = degree;
			qDebug() << "currentRotation =" << m_dRotationCur;
			setFormRotation(m_dRotationCur);
		}
}


