#include "Form1.h"
#include "ui_Form1.h"
#include <QMouseEvent>
#include <QDebug>

Form1::Form1(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::Form1)
{
	ui->setupUi(this);
}

Form1::~Form1()
{
	delete ui;
}

void Form1::mousePressEvent(QMouseEvent *event)
{
	m_bIsDragging = true;
	m_clDragStartPoint = event->pos();
	emit sigMousePress(m_clDragStartPoint);

}
void Form1::mouseMoveEvent(QMouseEvent *event)
{
	if(m_bIsDragging)
	{
		emit sigMouseMove(event->pos());
	}
}

void Form1::mouseReleaseEvent(QMouseEvent *event)
{
	if(m_bIsDragging)
	{
		m_bIsDragging = false;
		emit sigMouseRelease(event->pos());
	}
}

//void Form1::calcMouseMoveH(QPoint pos)
//{
//	int moveH = pos.x() - m_clDragStartPoint.x();
//	qDebug() << "MouseMoveH = " << moveH;
//	emit sigMouseMoveH(moveH);
//}

void Form1::on_pushButton_clicked()
{
	ui->lineEdit->setText("Hello Qt");
}
