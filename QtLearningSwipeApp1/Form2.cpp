#include "Form2.h"
#include "ui_Form2.h"
#include <QDebug>

Form2::Form2(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::Form2)
{
	ui->setupUi(this);
}

Form2::~Form2()
{
	delete ui;
}

void Form2::mousePressEvent(QMouseEvent *event)
{
	m_bIsDragging = true;
	m_clDragStartPoint = event->pos();
	emit sigMousePress(m_clDragStartPoint);

}
void Form2::mouseMoveEvent(QMouseEvent *event)
{
	if(m_bIsDragging)
	{
		emit sigMouseMove(event->pos());
	}
}

void Form2::mouseReleaseEvent(QMouseEvent *event)
{
	if(m_bIsDragging)
	{
		m_bIsDragging = false;
		emit sigMouseRelease(event->pos());
	}
}

//void Form2::calcMouseMoveH(QPoint pos)
//{
//	int moveH = pos.x() - m_clDragStartPoint.x();
//	qDebug() << "MouseMoveH = " << moveH;
//	emit sigMouseMoveH(moveH);
//}
