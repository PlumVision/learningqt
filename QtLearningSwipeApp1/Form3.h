#ifndef FORM3_H
#define FORM3_H

#include <QWidget>

namespace Ui {
class Form3;
}

class Form3 : public QWidget
{
	Q_OBJECT

public:
	explicit Form3(QWidget *parent = 0);
	~Form3();

protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);

signals:
	//void sigMouseMoveH(int width);	//水平方向のドラッグ(スワイプ)量を発信
	//void sigMouseRelease();
	void sigMousePress(QPoint);
	void sigMouseMove(QPoint);
	void sigMouseRelease(QPoint);

private:
	Ui::Form3 *ui;

	bool m_bIsDragging;	//!<ドラッグ中のフラグ
	QPoint m_clDragStartPoint;	//!<ドラッグ開始点

	void calcMouseMoveH(QPoint pos);
};

#endif // FORM3_H
