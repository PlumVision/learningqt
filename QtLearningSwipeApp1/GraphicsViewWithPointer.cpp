#include "GraphicsViewWithPointer.h"
#include <QMouseEvent>

GraphicsViewWithPointer::GraphicsViewWithPointer()
{

}

void GraphicsViewWithPointer::mousePressEvent(QMouseEvent *event)
{
	emit sigPress(event->pos());

	QGraphicsView::mousePressEvent(event);
}

void GraphicsViewWithPointer::mouseMoveEvent(QMouseEvent *event)
{
	emit sigMove(event->pos());

	QGraphicsView::mouseMoveEvent(event);
}

void GraphicsViewWithPointer::mouseReleaseEvent(QMouseEvent *event)
{
	emit sigRelease(event->pos());

	QGraphicsView::mouseReleaseEvent(event);
}

