#ifndef FORM2_H
#define FORM2_H

#include <QWidget>

namespace Ui {
class Form2;
}

class Form2 : public QWidget
{
	Q_OBJECT

public:
	explicit Form2(QWidget *parent = 0);
	~Form2();

protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);

signals:
//	void sigMouseMoveH(int width);	//水平方向のドラッグ(スワイプ)量を発信
//	void sigMouseRelease();
	void sigMousePress(QPoint);
	void sigMouseMove(QPoint);
	void sigMouseRelease(QPoint);

private:
	Ui::Form2 *ui;

	bool m_bIsDragging;	//!<ドラッグ中のフラグ
	QPoint m_clDragStartPoint;	//!<ドラッグ開始点

	void calcMouseMoveH(QPoint pos);
};

#endif // FORM2_H
