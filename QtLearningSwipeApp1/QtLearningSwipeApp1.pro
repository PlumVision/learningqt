#-------------------------------------------------
#
# Project created by QtCreator 2018-07-16T00:22:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtLearningSwipeApp1
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32:{
CONFIG(debug, debug|release){
	INCLUDEPATH += $(VLDROOT)/include
}
}

SOURCES += \
        main.cpp \
        MainWindow.cpp \
    Form1.cpp \
    Form2.cpp \
    Form3.cpp \
    GraphicsViewWithPointer.cpp

HEADERS += \
        MainWindow.h \
    Form1.h \
    Form2.h \
    Form3.h \
    GraphicsViewWithPointer.h

FORMS += \
        MainWindow.ui \
    Form1.ui \
    Form2.ui \
    Form3.ui

win32:{
CONFIG(debug, debug|release){
	DEFINES += _DEBUG
	LIBS += -L$(VLDROOT)/lib/Win64 \
			-lvld
}
}

	win32-msvc2015{
	INCLUDEPATH += "C:\Program Files (x86)\Windows Kits\10\Include\10.0.10240.0\ucrt"
	LIBS += -L"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.10240.0\ucrt\x64"
			-ucrtd

	}

CONFIG += mobility
MOBILITY = 

