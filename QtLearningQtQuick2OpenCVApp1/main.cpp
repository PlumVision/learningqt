﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <Camera.h>

int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);

	qmlRegisterType<CameraItem>("CameraItem", 1, 0, "Camera");

	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	return app.exec();
}
