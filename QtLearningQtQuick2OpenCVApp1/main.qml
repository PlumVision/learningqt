﻿import QtQuick 2.8
import QtQuick.Window 2.2
import CameraItem 1.0

Window {

    visible: true
    width: 640
    height: 480
    title: qsTr("QtQuick-OpenCV Cam")

    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log(qsTr('Clicked on background'))
            //console.log(qsTr('Clicked on background. Text: "' + textEdit.text + '"'))
        }
    }

    Rectangle{
        width: 640
        height: 480

        property alias camera: camera

        Camera {
            id:camera
            property int framerate: 30
            anchors.fill: parent
        }
    }
    Timer{
        interval: 1000 / camera.framerate
//        interval: 50

        running: true
        repeat: true
        onTriggered: {
            //console.log('framerate =', camera.framerate)
            camera.update()
        }
    }
//    TextEdit {
//        id: textEdit
//        text: qsTr("Enter some text...")
//        verticalAlignment: Text.AlignVCenter
//        anchors.top: parent.top
//        anchors.horizontalCenter: parent.horizontalCenter
//        anchors.topMargin: 20
//        Rectangle {
//            anchors.fill: parent
//            anchors.margins: -10
//            color: "transparent"
//            border.width: 1
//        }
//    }
}
