﻿#include "Camera.h"
#include <QImage>
#include <QDebug>

CameraItem::CameraItem(QQuickItem* parent):
	QQuickPaintedItem(parent), m_clCamera(0)
{

}

void CameraItem::paint(QPainter *painter)
{
	// get camera input
	 cv::Mat input_img;
	 m_clCamera >> input_img;

	 // BGR -> ARGB
	 cv::cvtColor(input_img, input_img, CV_BGR2BGRA);
	 std::vector<cv::Mat> bgra;
	 cv::split(input_img, bgra);
	 std::swap(bgra[0], bgra[3]);
	 std::swap(bgra[1], bgra[2]);

	 // cv::Mat から QImage へ変換、リサイズしてから描画
	 QImage output_img(input_img.data, input_img.cols, input_img.rows, QImage::Format_ARGB32);
	 output_img = output_img.scaled(width(), height());
	 painter->drawImage(width()/2  - output_img.size().width()/2,
						height()/2 - output_img.size().height()/2,
						output_img);
}

void CameraItem::repaint()
{
	qDebug() << "Hello";
}
