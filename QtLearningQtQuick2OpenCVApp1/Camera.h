﻿#ifndef CAMERA_H
#define CAMERA_H

#include <QQuickPaintedItem>
#include <opencv2/opencv.hpp>
#include <QPainter>

class CameraItem : public QQuickPaintedItem
{
	Q_OBJECT

public:
	CameraItem(QQuickItem* parent = 0);
	void paint(QPainter *painter);
	void repaint();

private:
	cv::VideoCapture m_clCamera;
};

#endif // CAMERA_H
