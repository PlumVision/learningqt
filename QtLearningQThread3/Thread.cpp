#include "Thread.h"
//#include <iostream>
#include <omp.h>
#include <QDebug>

Thread::Thread() : m_bStopped(false)
{
}
void Thread::calc()
{
	if(!isRunning())
		start();
}

void Thread::run()
{
	qint64 end = 1000000000;
	qint64 sum = 0;

	#pragma omp parallel for reduction(+:sum)
	for(qint64 i = 1; i <= end; i++)
	{
		sum += i;
		//std::cout << sum << ",";
	#ifdef _USE_PROGRESS
		emit progress(100 * static_cast<double>(i) / end);
	#endif
		if(m_bStopped)
		{
			qDebug() << "aborted";
			m_bStopped = false;
			break;
		}
	}
	qDebug() << sum ;
	emit finished();
}

void Thread::stop()
{
	m_bStopped = true;
}
