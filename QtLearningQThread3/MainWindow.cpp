#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QCloseEvent>

#include "Thread.h"
#include "ProgressDialog.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent), m_pclThread(NULL), m_pclProgress(NULL),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}
void MainWindow::closeEvent(QCloseEvent *event)
{
	if(m_pclThread != NULL && m_pclThread->isRunning())
	{
		event->ignore();
	}
	else
	{
		event->accept();
		qApp->quit();
	}
}

void MainWindow::on_action_Quit_triggered()
{
	this->close();
}

void MainWindow::on_btnCalc_clicked()
{
	if(m_pclThread != NULL && m_pclThread->isRunning())
	{
		return;
	}
	if(m_pclThread != NULL && m_pclThread->isFinished())
	{
		delete m_pclThread;
		m_pclThread = NULL;
	}
	m_pclThread = new Thread();
	QObject::connect(m_pclThread, SIGNAL(progress(int)),
					 this, SLOT(slotRecieveThreadProgress(int)));
	QObject::connect(m_pclThread, SIGNAL(finished()),
					 this, SLOT(slotRecieveThreadFinishied()));

	if(m_pclProgress != NULL)
	{
		delete m_pclProgress;
		m_pclProgress = NULL;
	}

	ui->statusBar->showMessage(tr("Calculating..."), 0);

	m_pclProgress = new ProgressDialog();
	m_pclProgress->setTitle("Now Calculating...");
#ifdef _USE_PROGRESS
	m_pclProgress->setRange(0, 100);
	m_pclProgress->setProgressValue(0);
#else
	m_pclProgress->setRange(0, 0);
	m_pclProgress->setProgressValue(-1);
#endif
	m_pclProgress->show();

	ui->action_Quit->setEnabled(false);

	m_clEt.restart();
	m_pclThread->calc();
}

void MainWindow::slotRecieveThreadFinishied()
{
	if(m_pclThread != NULL && m_pclThread->isRunning())
	{
		m_pclThread->stop();
		m_pclThread->wait();
	}
	qint64 elapsed = m_clEt.elapsed();
	ui->statusBar->showMessage(tr("%1 msec").arg(elapsed), 0);
	if(m_pclThread != NULL && m_pclThread->isFinished())
	{
		delete m_pclThread;
		m_pclThread = NULL;
	}

	if(m_pclProgress != NULL)
	{
		m_pclProgress->hide();
		delete m_pclProgress;
		m_pclProgress = NULL;
	}
	ui->action_Quit->setEnabled(true);
}

void MainWindow::on_btnAbort_clicked()
{
	if(m_pclThread != NULL && m_pclThread->isRunning())
	{
		m_pclThread->stop();
	}
}

void  MainWindow::slotRecieveThreadProgress(int val)
{
	if(NULL != m_pclProgress)
		m_pclProgress->setProgressValue(val);
}
