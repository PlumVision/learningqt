#include "ProgressDialog.h"
#include "ui_ProgressDialog.h"

ProgressDialog::ProgressDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ProgressDialog)
{
	ui->setupUi(this);
	setWindowFlags(Qt::Popup);
}

ProgressDialog::~ProgressDialog()
{
	delete ui;
}

void ProgressDialog::setTitle(const QString& title)
{
	QDialog::setWindowTitle(title);
}

void ProgressDialog::setProgressValue(int value)
{
	ui->progressBar->setValue(value);
}

void ProgressDialog::setRange(int min, int max)
{
	if (min > max) return;

	ui->progressBar->setRange(min, max);

}
