#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QElapsedTimer>
class Thread;
class ProgressDialog;



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

protected:
	void closeEvent(QCloseEvent *event);

private slots:
	void on_action_Quit_triggered();
	void on_btnCalc_clicked();
	void slotRecieveThreadProgress(int);
	void slotRecieveThreadFinishied();

	void on_btnAbort_clicked();

private:
	Ui::MainWindow *ui;
	Thread* m_pclThread;
	ProgressDialog* m_pclProgress;
	QElapsedTimer m_clEt;
};

#endif // MAINWINDOW_H
