#ifndef THREAD_H
#define THREAD_H

#include <QThread>

class Thread : public QThread
{
	Q_OBJECT
public:
	Thread();
	void calc();
	void stop();

signals:
	void finished();
	void progress(int);

protected:
	void run();

private:
	bool m_bStopped;
};

#endif // THREAD_H
