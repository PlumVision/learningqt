#-------------------------------------------------
#
# Project created by QtCreator 2017-08-24T18:41:04
#
#-------------------------------------------------

QT       += core gui
CONFIG += console

#QMAKE_CXXFLAGS += /openmp
#DEFINES += _USE_PROGRESS

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtLearningQThread3
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    ProgressDialog.cpp \
    Thread.cpp

HEADERS  += MainWindow.h \
    ProgressDialog.h \
    Thread.h

FORMS    += MainWindow.ui \
    ProgressDialog.ui
