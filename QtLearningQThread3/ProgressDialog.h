#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QDialog>

namespace Ui {
class ProgressDialog;
}

class ProgressDialog : public QDialog
{
	Q_OBJECT

public:
	explicit ProgressDialog(QWidget *parent = 0);
	~ProgressDialog();
	void setTitle(const QString& title);
	void setProgressValue(int value);
	void setRange(int min, int max);

private:
	Ui::ProgressDialog *ui;
};

#endif // PROGRESSDIALOG_H
