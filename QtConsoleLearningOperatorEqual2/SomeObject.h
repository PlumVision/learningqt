//https://ameblo.jp/woooh-p/entry-10037991084.html
#ifndef SOMEOBJECT_H
#define SOMEOBJECT_H
#include "Widget.h"
#include <QDebug>
#include <new>

//#define USE_UNIQUE_PTR
#ifdef USE_UNIQUE_PTR
#include <memory>
#endif

class SomeObject
{
public:

	SomeObject()
		: widget_1(new Widget)
		, widget_2(new Widget)
	{
		qDebug()  << __FUNCTION__ ;
	}

	~SomeObject()
	{
		qDebug()  << __FUNCTION__ ;
#ifndef USE_UNIQUE_PTR
		delete widget_2;
		delete widget_1;
#endif
	}

	void* operator new (size_t size)
	{
		void* p = ::operator new(size);
		qDebug()  << "SomeObject::" << __FUNCTION__ << ": p = " << p ;
		return p;
	}

	void operator delete(void* p)
	{
		qDebug()  << "SomeObject::" << __FUNCTION__ << ": p = " << p ;
		::operator delete(p);
	}

private:
#ifdef USE_UNIQUE_PTR
	std::unique_ptr<Widget> widget_1;
	std::unique_ptr<Widget> widget_2;
#else
	Widget* widget_1;
	Widget* widget_2;
#endif
};

#endif // SOMEOBJECT_H
