#include <QCoreApplication>
#include <QDebug>
#include <memory>
#include <exception>
#include "Widget.h"

#ifdef Q_OS_WIN
#ifdef _DEBUG
#include <vld.h>
#endif
#endif

template<class T>
class Sample
{
public:
	//デフォルトコンストラクタ
	Sample():m_pWidget(nullptr){}
	//デストラクタ
	~Sample(){delete m_pWidget;}
	//コピーコンストラクタ
	Sample(const Sample& rhs){
		if(rhs.m_pWidget == nullptr)
		{
			m_pWidget = nullptr;
		}
		else
		{
			m_pWidget = new Widget();
		}
		m_Member = rhs.m_Member;
	}
	//コピー代入演算子
	Sample& operator = (const Sample& rhs)
	{
		if(rhs.m_pWidget == nullptr)
		{
			delete m_pWidget;
			m_pWidget = nullptr;
		}
		else
		{
			Widget* pOrig = m_pWidget;
			m_pWidget = new Widget;
			delete pOrig;
		}
		m_Member = rhs.m_Member;

		return *this;
	}
	//widgetを生成するインタフェース
	int createWidget(T value)
	{
		try{
			Widget* pOrig = m_pWidget;
			m_pWidget = new Widget;
			delete pOrig;
		}
		catch(std::bad_alloc)
		{
			return -1;
		}

		m_Member = value;

		return 0;
	}

private:
	Widget* m_pWidget;
	T m_Member;
};

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	qDebug() << "Create w1(a instance of Widget) by default constructor";
	Widget w1;

	qDebug() << "Create w2(a instance of Widget) by copy constructor";
	Widget w2 = w1;

	qDebug() << "Create s1(a instance of Sample) by default constructor";
	Sample<int> s1;

	qDebug() << "Create a widget(a instance of Widget) in s1 by createWidget function";
	int ret = s1.createWidget(1);
	if(0 == ret)
		qDebug() << "createWidget succeeded.";
	else
		qDebug() << "createWidget failed.";

	ret = s1.createWidget(2);
	if(0 == ret)
		qDebug() << "createWidget succeeded.";
	else
		qDebug() << "createWidget failed.";

	qDebug() << "Create s2(a instance of Sample) by copy constructor";
#if 0
	try
	{
		Sample<int> s2 = s1;
		qDebug() << "address of s2 =" << &s2;
	}
	catch(std::exception& e)
	{
		qDebug() << e.what();
	}
#else
	Sample<int> s2 = s1;
#endif

	qDebug() << "Create s3(a instance of Sample) by default constructor";
	Sample<int> s3;

//	qDebug() << "copy s2 to s3 by operator '='";
//	s3 = s2;

	return 0;//a.exec();
}
