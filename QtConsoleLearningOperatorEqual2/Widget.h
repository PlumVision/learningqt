#ifndef WIDGET_H
#define WIDGET_H
#include <qDebug>
#include <new>

//#define NO_ERROR

class Widget
{
public:

	Widget()
	{
	qDebug() << __FUNCTION__ ;
	}
	~Widget()
	{
	qDebug() << __FUNCTION__ ;
	}

	void* operator new (size_t size)
	{
		static char chunk[sizeof(Widget)];
		static bool was_called = false;

		if (was_called) {
		#ifndef NO_ERROR
			throw std::bad_alloc();
		#endif
		}
		was_called = true;

		qDebug()  << "Widget::" << __FUNCTION__
		<< ": p = " << static_cast<void*>(chunk);

		return chunk;
	}

	void operator delete(void* p)
	{
		qDebug()  << "Widget::" << __FUNCTION__ << ": p = " << p ;
	}

};

#endif // WIDGET_H
