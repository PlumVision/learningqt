#include "Widget.h"
#include "ui_Widget.h"
#include "GrahicsViewReviced2.h"
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QGraphicsDropShadowEffect>
#include <QScrollBar>
#include <QDebug>


Widget::Widget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::Widget),
	m_pclGraphicsViewMain(new GraphicsViewReviced2),
	m_pclGraphicsViewSub(new QGraphicsView),
	m_pclShadow(new QGraphicsDropShadowEffect),
	m_pclGraphicsScene(new QGraphicsScene(this)),
	m_pclPixmapItem(nullptr),
	m_clPixmap(QPixmap("../QtMagnifyingViewTest1/img.jpg"))
{
	ui->setupUi(this);
	init();
}

Widget::~Widget()
{
	delete m_pclShadow;
	delete m_pclGraphicsViewSub;
	delete ui;
}

void Widget::init()
{
	setWindowTitle("QtMagnifyingViewTest1");
	float mag = 0.2;
	m_pclGraphicsViewMain->setMinimumWidth(m_clPixmap.width() * mag + 4);
	m_pclGraphicsViewMain->setMinimumHeight(m_clPixmap.height() * mag + 4);

	m_pclGraphicsViewSub->setMinimumWidth(sm_iSubViewWidth);
	m_pclGraphicsViewSub->setMinimumHeight(sm_iSubViewHeight);
	m_pclGraphicsViewSub->setMaximumWidth(sm_iSubViewWidth);
	m_pclGraphicsViewSub->setMaximumHeight(sm_iSubViewHeight);
	m_pclGraphicsViewSub->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_pclGraphicsViewSub->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	//m_pclGraphicsViewSub->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);

	ui->gridLayout->addWidget(m_pclGraphicsViewMain);

	m_pclGraphicsScene->setSceneRect(0, 0, m_clPixmap.width(), m_clPixmap.height());

	m_pclGraphicsViewMain->setScene(m_pclGraphicsScene);
	m_pclGraphicsViewSub->setScene(m_pclGraphicsScene);
	//m_pclGraphicsViewSub->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);


	m_pclPixmapItem = new QGraphicsPixmapItem(m_clPixmap);
	m_pclGraphicsScene->addItem(m_pclPixmapItem);

	//QTransform trans;
	//trans.scale(mag, mag);
	//m_pclGraphicsViewMain->setTransform(trans);
	QMatrix matrix;
	matrix.scale(mag, mag);
	//matrix.rotate(45);
	//matrix.translate(300,300);
	m_pclGraphicsViewMain->setMatrix(matrix);


	QObject::connect(m_pclGraphicsViewMain, &GraphicsViewReviced2::sigMousePress,
					 this, &Widget::slotMousePress);
	QObject::connect(m_pclGraphicsViewMain, &GraphicsViewReviced2::sigMouseMove,
					 this, &Widget::slotMouseMove);
	QObject::connect(m_pclGraphicsViewMain, &GraphicsViewReviced2::sigMouseRelease,
					 this, &Widget::slotMouseRelease);

	// エフェクト作成
	m_pclShadow->setColor( QColor( 0, 0, 0 ) );
	m_pclShadow->setOffset( 5, 5 );
	m_pclShadow->setBlurRadius( 15 );

	//m_pclPixmapItem->setGraphicsEffect(m_pclShadow);

	m_pclGraphicsViewSub->setGraphicsEffect(m_pclShadow);
	m_pclGraphicsViewSub->setWindowFlags(Qt::Window
										 | Qt::MSWindowsFixedSizeDialogHint
										 | Qt::CustomizeWindowHint);
}

void Widget::closeEvent(QCloseEvent *event)
{
	m_pclGraphicsViewSub->close();
}

void  Widget::slotMousePress(QPoint pt, int button)
{
	qDebug() << "Mouse Press ( x , y , button ) = (" << pt.x()  << ","
			 << pt.y() << "," << button << ")";
	QPointF scenePt = m_pclGraphicsViewMain->mapToScene(pt);
	qDebug() << "Scene pos ( x , y ) = (" << scenePt.x() << ","
			 << scenePt.y() << ")";
	m_clPointMousePress = pt;

	//表示範囲
#if 0
	//QMatrix matrix;
	qreal dx = scenePt.x() - sm_iSubViewWidth / 2;
	qreal dy = scenePt.y() - sm_iSubViewHeight / 2;
	//matrix.setMatrix(1.0, 0.0, 0.0, 1.0, dx, dy);
	QTransform trans;
	trans.translate(dx, dy);
	//m_pclGraphicsViewSub->setMatrix(matrix);
	m_pclGraphicsViewSub->setTransform(trans);
	qDebug() << "matrix.dx =" << dx << ", dy =" <<dy;
#else
	QScrollBar* pVBar = m_pclGraphicsViewSub->verticalScrollBar();
	QScrollBar* pHBar = m_pclGraphicsViewSub->horizontalScrollBar();

	int maxV = pVBar->maximum();
	int maxH = pHBar->maximum();

	int valV = static_cast<int>(scenePt.y() - sm_iSubViewHeight / 2);
	int valH = static_cast<int>(scenePt.x() - sm_iSubViewWidth / 2);

	if(valV < 0) valV = 0;
	if(valV > maxV) valV = maxV;
	if(valH < 0) valH = 0;
	if(valH > maxH) valH = maxH;
	qDebug() << "valH:" << valH << "valV:" << valH;

	pVBar->setValue(valV);
	pHBar->setValue(valH);



#endif

	//表示位置
	QRect widgetRect = this->geometry();
	QRect viewMainRect = m_pclGraphicsViewMain->geometry();
	QRect viewSubRect(widgetRect.left() + viewMainRect.left() + pt.x() - sm_iSubViewWidth / 2,
					  widgetRect.top() +viewMainRect.top() + pt.y() - sm_iSubViewHeight / 2,
				   sm_iSubViewWidth, sm_iSubViewHeight);
	m_pclGraphicsViewSub->setGeometry(viewSubRect);
	m_pclGraphicsViewSub->show();
	m_pclGraphicsViewSub->raise();
}

void  Widget::slotMouseMove(QPoint pt, int button)
{
	qDebug() << "Mouse Move ( x , y , button ) = (" << pt.x()  << ","
			 << pt.y() << "," << button << ")";
	QPointF scenePt = m_pclGraphicsViewMain->mapToScene(pt);
	qDebug() << "Scene pos ( x , y ) = (" << scenePt.x() << ","
			 << scenePt.y() << ")";

	//表示範囲
#if 0
	//QMatrix matrix;
	qreal dx = scenePt.x() - sm_iSubViewWidth / 2;
	qreal dy = scenePt.y() - sm_iSubViewHeight / 2;
	//matrix.setMatrix(1.0, 0.0, 0.0, 1.0, dx, dy);
	QTransform trans;
	trans.translate(dx, dy);
	//m_pclGraphicsViewSub->setMatrix(matrix);
	m_pclGraphicsViewSub->setTransform(trans);
	qDebug() << "matrix.dx =" << dx << ", dy =" <<dy;
#else
	QScrollBar* pVBar = m_pclGraphicsViewSub->verticalScrollBar();
	QScrollBar* pHBar = m_pclGraphicsViewSub->horizontalScrollBar();

	int maxV = pVBar->maximum();
	int maxH = pHBar->maximum();

	int valV = static_cast<int>(scenePt.y() - sm_iSubViewHeight / 2);
	int valH = static_cast<int>(scenePt.x() - sm_iSubViewWidth / 2);

	if(valV < 0) valV = 0;
	if(valV > maxV) valV = maxV;
	if(valH < 0) valH = 0;
	if(valH > maxH) valH = maxH;
	qDebug() << "valH:" << valH << "valV:" << valH;

	pVBar->setValue(valV);
	pHBar->setValue(valH);

#endif

	QRect widgetRect = this->geometry();
	QRect viewMainRect = m_pclGraphicsViewMain->geometry();
	QRect viewSubRect(widgetRect.left() + viewMainRect.left() + pt.x() - sm_iSubViewWidth / 2,
					  widgetRect.top() +viewMainRect.top() + pt.y() - sm_iSubViewHeight / 2,
				   sm_iSubViewWidth, sm_iSubViewHeight);
	m_pclGraphicsViewSub->setGeometry(viewSubRect);


}

void Widget::slotMouseRelease(QPoint pt)
{
	qDebug() << "Mouse Release ( x , y ) = (" << pt.x()  << ","
		 << pt.y() << ")";
	m_pclGraphicsViewSub->hide();
}

