#ifndef WIDGET_H
#define WIDGET_H

class GraphicsViewReviced2;
class QGraphicsView;
class QGraphicsScene;
class QGraphicsPixmapItem;
class QGraphicsDropShadowEffect;

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
	Q_OBJECT

public:
	explicit Widget(QWidget *parent = nullptr);
	~Widget();

protected:
	void closeEvent(QCloseEvent *event);

private slots:
	void slotMousePress(QPoint, int);
	void slotMouseMove(QPoint, int);
	void slotMouseRelease(QPoint);

private:

	void init();

	GraphicsViewReviced2* m_pclGraphicsViewMain;
	QGraphicsView* m_pclGraphicsViewSub;
	QGraphicsDropShadowEffect* m_pclShadow;
	QGraphicsScene* m_pclGraphicsScene;
	QGraphicsPixmapItem* m_pclPixmapItem;
	QPixmap m_clPixmap;

	QPoint m_clPointMousePress;

	static const int sm_iSubViewWidth = 320;
	static const int sm_iSubViewHeight = 240;

	Ui::Widget *ui;
};

#endif // WIDGET_H
