#ifndef GRAHICSVIEWREVICED2_H
#define GRAHICSVIEWREVICED2_H

#include <QGraphicsView>
#include <QMouseEvent>

class GraphicsViewReviced2 : public QGraphicsView
{
	Q_OBJECT

public:
	explicit GraphicsViewReviced2(QGraphicsView *parent = nullptr)
		: QGraphicsView(parent)
	{


	}

signals:
	void sigMousePress(QPoint, int);
	void sigMouseMove(QPoint, int);
	void sigMouseRelease(QPoint);

protected:
	void mousePressEvent(QMouseEvent* event) override
	{
		emit sigMousePress(event->pos(), event->button());

		QGraphicsView::mousePressEvent(event);
	}

	void mouseMoveEvent(QMouseEvent* event) override
	{
		emit sigMouseMove(event->pos(), event->button());

		QGraphicsView::mouseMoveEvent(event);
	}

	void mouseReleaseEvent(QMouseEvent *event)
	{
		emit sigMouseRelease(event->pos());

		QGraphicsView::mouseReleaseEvent(event);
	}

private:

};

#endif // GRAHICSVIEWREVICED2_H
